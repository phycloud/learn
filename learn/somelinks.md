# web link

+ c++参考手册 https://zh.cppreference.com/w/cpp

+ 走进自动驾驶横向控制，你需要这篇超长综述 https://zhuanlan.zhihu.com/p/46377932

+ nms算法的原理及其基于tensorflow的实现 https://cloud.tencent.com/developer/article/1486383

+ 相机IMU融合四部曲（三）:MSF详细解读与使用  https://www.cnblogs.com/ilekoaiq/p/9311357.html

+ 无人驾驶算法学习（六）：多传感器融合MSF算法  https://blog.csdn.net/orange_littlegirl/article/details/89889453

+ Apollo代码学习(三)—车辆动力学模型  https://blog.csdn.net/u013914471/article/details/83018664

+ 无人驾驶算法学习（四）：多传感器融合(标定, 数据融合, 任务融合) https://blog.csdn.net/orange_littlegirl/article/details/89067017

+ Apollo的感知融合模块解析  https://blog.csdn.net/u012423865/article/details/80386444

+ 无迹卡尔曼滤波UKF_代码及调参(2)  https://blog.csdn.net/weixin_38451800/article/details/85721991

+ 目标检测|YOLOv2原理与实现(附YOLOv3) https://zhuanlan.zhihu.com/p/35325884

+ Protobuf有没有比JSON快5倍？用代码来击破pb性能神话  http://www.sohu.com/a/136487507_505779

+ RT-SLAM  https://www.openrobots.org/wiki/rtslam/

+ SIMD简介  https://zhuanlan.zhihu.com/p/55327037

+ deepo  https://github.com/ufoym/deepo

+ 相机标定原理介绍（一）  https://www.cnblogs.com/Jessica-jie/p/6596450.html

+ 激光雷达原理  https://wenku.baidu.com/view/6e8d72747fd5360cba1adb37.html

+ astuff_sensor_msgs  https://github.com/astuff/astuff_sensor_msgs

+ TensorRT部署深度学习模型 https://zhuanlan.zhihu.com/p/84125533

+ 高性能深度学习支持引擎实战——TensorRT  https://zhuanlan.zhihu.com/p/35657027

+ 图像处理：opencv的目标追踪方法总结  http://shartoo.github.io/opencv_obj_track/

+ opengithub新项目快报  https://www.open-open.com/github/view/github2018-12-25.html

+ Learning to Track at 100 FPS with Deep Regression Networks  https://blog.csdn.net/autocyz/article/details/52648776

+ 目标检测-20种常用深度学习算法论文、复现代码汇总  https://cloud.tencent.com/developer/article/1528846

+ Multi-Object-Tracking-Paper-List  https://blog.csdn.net/fly_wt/article/details/96477039

+ 多传感器融合MSF算法源码阅读(一)  https://blog.csdn.net/orange_littlegirl/article/details/96168174