# multi-process and multi-thread test with ROS

#### environment

computer:  

| item        | value              |
| ----------- | ------------------ |
| cpu number  | 20 physical thread |
| DDR memory  | 64GB               |
| hard memory | SSD                |
| cpu bits    | 64 bits            |

OS:

| item         | value        |
| ------------ | ------------ |
| kernel       | Linux 4.15.0 |
| distribution | Ubuntu 16.04 |
| ROS 1.0      | Kinetic      |

test method: **Image data send and receive test.**

| item              | value      |
| ----------------- | ---------- |
| publisher number  | 1          |
| subscriber number | 100        |
| image size        | 1280 * 720 |
| image freq        | 30 Hz      |

#### test and result

| item           | Multi-process | Multi-thread |
| -------------- | ------------- | ------------ |
| pub node       | 1             | 1            |
| sub node       | 100           | 1            |
| thread per sub | 1             | 100          |
| launch speed   | slow          | fast         |
| quit speed     | slow          | fast         |
| total memory   | 1.8~1.9 GB    | 0.5 GB       |
| mem of pub     | 42~45 MB      | 42~45 MB     |
| mem of per sub | 18~19 MB      | 18~19 MB     |

在相同的硬件环境和阮江环境下，采用一个节点作为图像发送节点，Multi-process采用100各节点，对应100个进程作为接收，Multi-thread采用1各节点，对应100个线程作为接收，对比两种方案的测试结果：
Multi-process方案启动和退出的时间较长，消耗内存较多；
Multi-thread方案启动和退出的时间较短，消耗内存较少；
从安全性的角度，Multi-process方案比Multi-thread方案线程之间无影响，任务相互独立，安全性更高。