# install LXDE on ubuntu base 16.04

sudo apt-get install lxde

Welcome to Ubuntu 16.04.6 LTS!

[    4.882647] input: Logitech USB Receiver as /devices/platform/amba/ff9d0000.usb0/fe200000.dwc3/xhci-hcd.0.auto/usb1/1-1/1-1.2/1-1.2:1.1/0003:046D:C534.0002/input/input2
[    4.905907] systemd[1]: Set hostname to <zcu102>.
[    4.960466] hid-generic 0003:046D:C534.0002: input: USB HID v1.11 Mouse [Logitech USB Receiver] on usb-xhci-hcd.0.auto-1.2/input1
[    5.110104] systemd[1]: Listening on /dev/initctl Compatibility Named Pipe.
[  OK  ] Listening on /dev/initctl Compatibility Named Pipe.
[    5.136437] systemd[1]: Started Forward Password Requests to Wall Directory Watch.
[  OK  ] Started Forward Password Requests to Wall Directory Watch.
[    5.164329] systemd[1]: Reached target Remote File Systems (Pre).
[  OK  ] Reached target Remote File Systems (Pre).
[    5.184336] systemd[1]: Reached target Remote File Systems.
[  OK  ] Reached target Remote File Systems.
[    5.204412] systemd[1]: Listening on udev Control Socket.
[  OK  ] Listening on udev Control Socket.
[    5.224330] systemd[1]: Reached target Swap.
[  OK  ] Reached target Swap.
[    5.240388] systemd[1]: Listening on Journal Socket (/dev/log).
[  OK  ] Listening on Journal Socket (/dev/log).
[  OK  ] Reached target User and Group Name Lookups.
[  OK  ] Created slice User and Session Slice.
[  OK  ] Created slice System Slice.
[  OK  ] Reached target Slices.
[  OK  ] Created slice system-serial\x2dgetty.slice.
[  OK  ] Listening on Syslog Socket.
[  OK  ] Reached target Encrypted Volumes.
[  OK  ] Listening on Journal Audit Socket.
[  OK  ] Listening on Journal Socket.
         Starting Journal Service...
         Starting Load Kernel Modules...
         Mounting Huge Pages File System...
         Starting Set console keymap...
         Mounting POSIX Message Queue File System...
         Starting Create Static Device Nodes in /dev...
         Mounting Debug File System...
         Starting Remount Root and Kernel File Systems...
[  OK  ] Listening on udev Kernel Socket.
[  OK  ] Mounted Huge Pages File System.
[  OK  ] Mounted POSIX Message Queue File System.
[  OK  ] Mounted Debug File System.
[  OK  ] Started Journal Service.
[  OK  ] Started Load Kernel Modules.
[FAILED] Failed to start Set console keymap.
See 'systemctl status keyboard-setup.service' for details.
[  OK  ] Started Create Static Device Nodes in /dev.
[  OK  ] Started Remount Root and Kernel File Systems.
         Starting udev Coldplug all Devices...
         Starting Load/Save Random Seed...
         Starting udev Kernel Device Manager...
[  OK  ] Reached target Local File Systems (Pre).
[  OK  ] Reached target Local File Systems.
         Starting Set console font and keymap...
         Starting Tell Plymouth To Write Out Runtime Data...
         Starting Apply Kernel Variables...
         Mounting Configuration File System...
         Starting Flush Journal to Persistent Storage...
[  OK  ] Mounted Configuration File System.
[  OK  ] Started Load/Save Random Seed.
[FAILED] Failed to start Set console font and keymap.
See 'systemctl status console-setup.service' for details.
[  OK  ] Started Tell Plymouth To Write Out Runtime Data.
[  OK  ] Started Apply Kernel Variables.
[  OK  ] Started udev Kernel Device Manager.
         Starting Raise network interfaces...
[  OK  ] Created slice system-getty.slice.
[  OK  ] Started Flush Journal to Persistent Storage.
         Starting Create Volatile Files and Directories...
[  OK  ] Started Create Volatile Files and Directories.
         Starting Network Time Synchronization...
         Starting Update UTMP about System Boot/Shutdown...
[  OK  ] Started Network Time Synchronization.
[  OK  ] Reached target System Time Synchronized.
[  OK  ] Started Update UTMP about System Boot/Shutdown.
[  OK  ] Reached target Sound Card.
[  OK  ] Found device /dev/ttyPS0.
[  OK  ] Started udev Coldplug all Devices.
[  OK  ] Reached target System Initialization.
[  OK  ] Listening on D-Bus System Message Bus Socket.
[  OK  ] Started Daily Cleanup of Temporary Directories.
[  OK  ] Started Daily apt download activities.
[  OK  ] Started Daily apt upgrade and clean activities.
[  OK  ] Reached target Timers.
[  OK  ] Listening on Avahi mDNS/DNS-SD Stack Activation Socket.
[  OK  ] Reached target Sockets.
[  OK  ] Started Dispatch Password Requests to Console Directory Watch.
[  OK  ] Reached target Paths.
[  OK  ] Reached target Basic System.
         Starting LSB: Set the CPU Frequency Scaling governor to "ondemand"...
         Starting Accounts Service...
         Starting Permit User Sessions...
         Starting Login Service...
         Starting System Logging Service...
[  OK  ] Started Regular background program processing daemon.
         Starting Avahi mDNS/DNS-SD Stack...
         Starting Restore /etc/resolv.conf i...re the ppp link was shut down...
[  OK  ] Started D-Bus System Message Bus.
[  OK  ] Started Avahi mDNS/DNS-SD Stack.
         Starting Network Manager...
[  OK  ] Started System Logging Service.
[  OK  ] Started Permit User Sessions.
[  OK  ] Started Restore /etc/resolv.conf if...fore the ppp link was shut down.
[  OK  ] Started LSB: Set the CPU Frequency Scaling governor to "ondemand".
[  OK  ] Started Network Manager.
[  OK  ] Started Login Service.
         Starting Network Manager Script Dispatcher Service...
         Starting Authenticate and Authorize Users to Run Privileged Tasks...
[  OK  ] Listening on Load/Save RF Kill Switch Status /dev/rfkill Watch.
[  OK  ] Started ifup for eth0.
         Starting Light Display Manager...
[  OK  ] Started Network Manager Script Dispatcher Service.
[  OK  ] Started Authenticate and Authorize Users to Run Privileged Tasks.
[  OK  ] Started Accounts Service.
[  OK  ] Started Light Display Manager.
         Starting Hostname Service...
[FAILED] Failed to start Hostname Service.
See 'systemctl status systemd-hostnamed.service' for details.
[  OK  ] Stopped Light Display Manager.
         Starting Light Display Manager...
[  OK  ] Started Light Display Manager.
[  OK  ] Stopped Light Display Manager.
         Starting Light Display Manager...
[  OK  ] Started Light Display Manager.
[  OK  ] Stopped Light Display Manager.
         Starting Light Display Manager...
[  OK  ] Started Light Display Manager.
[  OK  ] Stopped Light Display Manager.
         Starting Light Display Manager...
[  OK  ] Started Light Display Manager.
[  OK  ] Stopped Light Display Manager.
[FAILED] Failed to start Light Display Manager.
See 'systemctl status lightdm.service' for details.
[  OK  ] Started Raise network interfaces.
[  OK  ] Reached target Network.
         Starting OpenBSD Secure Shell server...
         Starting /etc/rc.local Compatibility...
[  OK  ] Started /etc/rc.local Compatibility.
         Starting Hold until boot process finishes up...
[  OK  ] Started Hold until boot process finishes up.
[  OK  ] Started Getty on tty1.
[  OK  ] Started Serial Getty on ttyPS0.
[  OK  ] Started Getty on ttymxc0.
[  OK  ] Reached target Login Prompts.
[  OK  ] Started OpenBSD Secure Shell server.
[  OK  ] Reached target Multi-User System.
[  OK  ] Reached target Graphical Interface.
         Starting Update UTMP about System Runlevel Changes...
[  OK  ] Started Update UTMP about System Runlevel Changes.

Ubuntu 16.04.6 LTS zcu102 ttyPS0

## seperate

 systemctl status keyboard-setup.service

//看哪里错了：
systemctl status console-setup.service
//解决错误：
sudo systemctl restart console-setup.service
//看解决没有：
systemctl status console-setup.service 



gnome3.8 sudo add-apt-repository ppa:gnome3-team/gnome3 sudo apt-get update sudo apt-get install gnome-shell ubuntu-gnome-desktop unity桌面 sudo apt-get install ubuntu-desktop kde