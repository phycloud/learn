# NVIDIA 驱动问题

+ 原本nvidia驱动运行正常，突然出现问题，可能存在依赖库自动更新，导致driver无法工作，解决办法. `sudo vi /etc/apt/apt.conf.d/50unattended-upgrades` 注释掉一下文件，禁止自动更新；

  ```
  Unattended-Upgrade::Allowed-Origins {
          //"${distro_id}:${distro_codename}";
          //"${distro_id}:${distro_codename}-security";
          // Extended Security Maintenance; doesn't necessarily exist for
          // every release and this system may not have it installed, but if
          // available, the policy for updates is such that unattended-upgrades
          // should also install from here by default.
          //"${distro_id}ESM:${distro_codename}";
  //      "${distro_id}:${distro_codename}-updates";
  //      "${distro_id}:${distro_codename}-proposed";
  //      "${distro_id}:${distro_codename}-backports";
  };
  ```

  