# install ROS-melodic on Fedora 28 aarch64

### update repo:

```
cd /etc/yum.repos.d
sudo mv fedora.repo fedora-metalink.repo 
sudo mv fedora-updates.repo fedora-updates-metalink.repo
sudo wget -O /etc/yum.repos.d/fedora.repo http://mirrors.aliyun.com/repo/fedora.repo
sudo wget -O /etc/yum.repos.d/fedora-updates.repo http://mirrors.aliyun.com/repo/fedora-updates.repo
sudo dnf clean all
sudo dnf makecache
```



### install by source 

install basic depends:

```
sudo dnf install gcc-c++ python-rosdep python3-rosinstall_generator python-wstool python-rosinstall @buildsys-build
```

install python tools:

```
sudo pip install -U rosdep rosinstall_generator wstool rosinstall
```

```
sudo pip install --upgrade setuptools
```

install rosdep:

```
sudo rosdep init
rosdep update
```

```
mkdir ~/ros_catkin_ws
cd ~/ros_catkin_ws
```

```
rosinstall_generator desktop --rosdistro melodic --deps --tar > melodic-desktop.rosinstall
wstool init -j8 src melodic-desktop.rosinstall
```

resolve dependence:

```
rosdep install --from-paths src --ignore-src --rosdistro melodic -y
```

there is an error:

```
ERROR: Rosdep experienced an error: rosdep OS definition for [python-opencv:fedora] must be a dictionary, string, or list: None
Please go to the rosdep page [1] and file a bug report with the stack trace below.
[1] : http://www.ros.org/wiki/rosdep

rosdep version: 0.15.2

Traceback (most recent call last):
  File "/usr/lib/python3.7/site-packages/rosdep2/main.py", line 140, in rosdep_main
    exit_code = _rosdep_main(args)
  File "/usr/lib/python3.7/site-packages/rosdep2/main.py", line 396, in _rosdep_main
    return _package_args_handler(command, parser, options, args)
  File "/usr/lib/python3.7/site-packages/rosdep2/main.py", line 492, in _package_args_handler
    return command_handlers[command](lookup, packages, options)
  File "/usr/lib/python3.7/site-packages/rosdep2/main.py", line 691, in command_install
    uninstalled, errors = installer.get_uninstalled(packages, implicit=options.recursive, verbose=options.verbose)
  File "/usr/lib/python3.7/site-packages/rosdep2/installers.py", line 445, in get_uninstalled
    resolutions, errors = self.lookup.resolve_all(resources, installer_context, implicit=implicit)
  File "/usr/lib/python3.7/site-packages/rosdep2/lookup.py", line 401, in resolve_all
    self.resolve(rosdep_key, resource_name, installer_context)
  File "/usr/lib/python3.7/site-packages/rosdep2/lookup.py", line 485, in resolve
    installer_key, rosdep_args_dict = definition.get_rule_for_platform(os_name, os_version, installer_keys, default_key)
  File "/usr/lib/python3.7/site-packages/rosdep2/lookup.py", line 148, in get_rule_for_platform
    raise InvalidData('rosdep OS definition for [%s:%s] must be a dictionary, string, or list: %s' % (self.rosdep_key, os_name, data), origin=self.origin)
rosdep2.core.InvalidData: rosdep OS definition for [python-opencv:fedora] must be a dictionary, string, or list: None
```

based on https://github.com/ros-infrastructure/rosdep/issues/689, then instead:

```
rosdep install --from-paths src --ignore-src --rosdistro melodic -y --skip-keys=python-opencv
```

error:

```
ros_catkin_ws]$ rosdep install --from-paths src --ignore-src --rosdistro melodic -y --skip-keys=python-opencv
executing command [sudo -H dnf --assumeyes --setopt=strict=0 install boost-python2-devel]
Repository updates is listed more than once in the configuration
Repository updates-debuginfo is listed more than once in the configuration
Repository updates-source is listed more than once in the configuration
Repository fedora is listed more than once in the configuration
Repository fedora-debuginfo is listed more than once in the configuration
Repository fedora-source is listed more than once in the configuration
Last metadata expiration check: 1:11:47 ago on Thu 05 Sep 2019 06:38:43 AM EDT.
No match for argument: boost-python2-devel
Dependencies resolved.
Nothing to do.
Complete!
ERROR: the following rosdeps failed to install
  dnf: Failed to detect successful installation of [boost-python2-devel]
```

```
rosdep install -ay --os=fedora:28
```



```
sudo pip install -U rosdep
```

```
sudo rosdep init
```

```
rosdep update
```

```
./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release --install-space /opt/ros/melodic
```

error:

```
cv_bridge can't find "//include"
```

~~change cv_bridge source CMakeLists, add OpenCV_INCLUDE_DIRS definition before find_package(OpenCV): set(OpenCV_INCLUDE_DIRS /usr/include/opencv)~~

reference: https://github.com/opencv/opencv/issues/14323

```
source ~/ros_catkin_ws/install_isolated/setup.bash
```

