# tensorflow那些坑 c++

以下是tensorflow用C++部署时的一些坑，且行且踩坑！

## 编译篇



## 部署篇

1. 解决GPU显存占用过高的问题

   c++运行tensorflow推理模型时，出现显存占满的问题，原因是建立session时需要对GPU进行配置

   ```c++
   #include "tensorflow/core/protobuf/config.pb.h"
   
   tensorflow::SessionOptions options_;
   std::unique_ptr<tensorflow::Session> session_;
   tensorflow::GPUOptions gpu_options_;
   // 允许TF根据需要动态申请GPU显存
   gpu_options_.set_allow_growth(true);
   // 设置申请显存上线值为 20%
   gpu_options_.set_per_process_gpu_memory_fraction(0.2);
   // options_.target = "TF";
   // 添加GPU设置
   options_.config.set_allocated_gpu_options(&gpu_options_);
   // 创建新的session
   session_.reset(tensorflow::NewSession(options_));
   ```

   

2. 



