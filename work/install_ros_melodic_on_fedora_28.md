# install ros molodic on fedora 28

```
dnf updateinfo
```

````
dnf install gcc
````

```
Last metadata expiration check: 0:13:30 ago on Sat Sep 21 22:31:56 2019.
Dependencies resolved.
=================================================================
 Package                Arch    Version            Repository
                                                            Size
=================================================================
Installing:
 gcc                    aarch64 8.3.1-2.fc28       updates  19 M
Upgrading:
 glibc                  aarch64 2.27-38.fc28       updates 3.4 M
 glibc-common           aarch64 2.27-38.fc28       updates 724 k
 glibc-langpack-en      aarch64 2.27-38.fc28       updates 811 k
 glibc-minimal-langpack aarch64 2.27-38.fc28       updates  40 k
 libgcc                 aarch64 8.3.1-2.fc28       updates  96 k
 libxcrypt              aarch64 4.4.6-1.fc28       updates 126 k
Installing dependencies:
 binutils               aarch64 2.29.1-23.fc28     updates 6.2 M
 cpp                    aarch64 8.3.1-2.fc28       updates 8.6 M
 glibc-devel            aarch64 2.27-38.fc28       updates 1.0 M
 glibc-headers          aarch64 2.27-38.fc28       updates 451 k
 isl                    aarch64 0.16.1-6.fc28      fedora  790 k
 kernel-headers         aarch64 5.0.16-100.fc28    updates 1.2 M
 libasan                aarch64 8.3.1-2.fc28       updates 408 k
 libatomic              aarch64 8.3.1-2.fc28       updates  47 k
 libgomp                aarch64 8.3.1-2.fc28       updates 209 k
 libmpc                 aarch64 1.0.2-9.fc28       fedora   58 k
 libstdc++              aarch64 8.3.1-2.fc28       updates 466 k
 libubsan               aarch64 8.3.1-2.fc28       updates 166 k
 libxcrypt-devel        aarch64 4.4.6-1.fc28       updates  35 k

Transaction Summary
=================================================================
Install  14 Packages
Upgrade   6 Packages

Total download size: 44 M

```

```
dnf install gcc-c++ python-rosdep python3-rosinstall_generator python-wstool python-rosinstall @buildsys-build
```

```
Last metadata expiration check: 3:22:40 ago on Sat Sep 21 22:31:56 2019.
Dependencies resolved.
===============================================================================================================================================================
 Group                                                      Packages                                                                                          
===============================================================================================================================================================
Marking packages as installed by the group:
 @Buildsystem building group                                tar                                    bzip2                                     grep             
                                                            rpm-build                              diffutils                                 gcc-c++          
                                                            patch                                  xz                                        gzip             
                                                            bash                                   make                                      redhat-rpm-config
                                                            info                                   coreutils                                 gawk             
                                                            unzip                                  util-linux                                sed              
                                                            cpio                                   shadow-utils                              fedora-release   
                                                            gcc                                    which                                     findutils        
===============================================================================================================================================================
 Package                                          Arch                        Version                                        Repository                   Size
===============================================================================================================================================================
Installing:
 python2-rosdep                                   noarch                      0.11.4-9.fc28                                  fedora                      307 k
 python2-rosinstall                               noarch                      0.7.7-9.fc28                                   fedora                      242 k
 python2-wstool                                   noarch                      0.1.17-3.fc28                                  fedora                       93 k
 python3-rosinstall_generator                     noarch                      0.1.14-1.fc28                                  fedora                       30 k
Upgrading:
 file-libs                                        aarch64                     5.33-10.fc28                                   updates                     539 k
Installing group packages:
 bzip2                                            aarch64                     1.0.6-26.fc28                                  fedora                       58 k
 cpio                                             aarch64                     2.12-7.fc28                                    fedora                      259 k
 findutils                                        aarch64                     1:4.6.0-19.fc28                                updates                     523 k
 gcc-c++                                          aarch64                     8.3.1-2.fc28                                   updates                      10 M
 make                                             aarch64                     1:4.2.1-6.fc28                                 fedora                      491 k
 patch                                            aarch64                     2.7.6-8.fc28                                   updates                     133 k
 redhat-rpm-config                                noarch                      110-1.fc28                                     updates                      78 k
 rpm-build                                        aarch64                     4.14.2.1-2.fc28                                updates                     161 k
 unzip                                            aarch64                     6.0-38.fc28                                    fedora                      184 k
 which                                            aarch64                     2.21-8.fc28                                    fedora                       47 k
 xz                                               aarch64                     5.2.4-2.fc28                                   updates                     148 k
Installing dependencies:
 annobin                                          aarch64                     5.7-3.fc28                                     updates                      86 k
 apr                                              aarch64                     1.6.3-5.fc28                                   fedora                      119 k
 apr-util                                         aarch64                     1.6.1-8.fc28                                   updates                     103 k
 bzr                                              aarch64                     2.7.0-20.fc28                                  fedora                      6.4 M
 dwz                                              aarch64                     0.12-7.fc28                                    fedora                      102 k
 elfutils                                         aarch64                     0.174-5.fc28                                   updates                     322 k
 emacs-filesystem                                 noarch                      1:26.1-3.fc28                                  updates                      68 k
 file                                             aarch64                     5.33-10.fc28                                   updates                      75 k
 fipscheck                                        aarch64                     1.5.0-4.fc28                                   fedora                       26 k
 fipscheck-lib                                    aarch64                     1.5.0-4.fc28                                   fedora                       14 k
 fpc-srpm-macros                                  noarch                      1.1-4.fc28                                     fedora                      7.5 k
 gc                                               aarch64                     7.6.4-3.fc28                                   fedora                       97 k
 gdb-headless                                     aarch64                     8.1.1-4.fc28                                   updates                     3.0 M
 gdbm                                             aarch64                     1:1.14.1-4.fc28                                fedora                      119 k
 ghc-srpm-macros                                  noarch                      1.4.2-7.fc28                                   fedora                      8.2 k
 git                                              aarch64                     2.17.2-2.fc28                                  updates                     221 k
 git-core                                         aarch64                     2.17.2-2.fc28                                  updates                     4.0 M
 git-core-doc                                     noarch                      2.17.2-2.fc28                                  updates                     2.3 M
 gnat-srpm-macros                                 noarch                      4-5.fc28                                       fedora                      8.8 k
 go-srpm-macros                                   noarch                      2-16.fc28                                      fedora                       13 k
 groff-base                                       aarch64                     1.22.3-15.fc28                                 fedora                      979 k
 guile                                            aarch64                     5:2.0.14-7.fc28                                fedora                      3.5 M
 less                                             aarch64                     530-1.fc28                                     fedora                      160 k
 libatomic_ops                                    aarch64                     7.6.2-3.fc28                                   fedora                       36 k
 libbabeltrace                                    aarch64                     1.5.4-2.fc28                                   fedora                      187 k
 libedit                                          aarch64                     3.1-23.20170329cvs.fc28                        fedora                       97 k
 libsecret                                        aarch64                     0.18.6-1.fc28                                  fedora                      158 k
 libserf                                          aarch64                     1.3.9-6.fc28                                   fedora                       57 k
 libsodium                                        aarch64                     1.0.17-1.fc28                                  updates                     113 k
 libstdc++-devel                                  aarch64                     8.3.1-2.fc28                                   updates                     2.1 M
 libtool-ltdl                                     aarch64                     2.4.6-24.fc28                                  updates                      56 k
 mercurial                                        aarch64                     4.4.2-4.fc28                                   fedora                      4.2 M
 nim-srpm-macros                                  noarch                      1-1.fc28                                       fedora                      7.6 k
 ocaml-srpm-macros                                noarch                      5-2.fc27                                       fedora                      7.8 k
 openblas-srpm-macros                             noarch                      2-2.fc27                                       fedora                      6.6 k
 openssh                                          aarch64                     7.8p1-4.fc28                                   updates                     469 k
 openssh-clients                                  aarch64                     7.8p1-4.fc28                                   updates                     608 k
 perl-Carp                                        noarch                      1.42-396.fc28                                  updates                      29 k
 perl-Data-Dumper                                 aarch64                     2.167-399.fc28                                 fedora                       56 k
 perl-Digest                                      noarch                      1.17-395.fc28                                  fedora                       26 k
 perl-Digest-MD5                                  aarch64                     2.55-397.fc28                                  updates                      36 k
 perl-Encode                                      aarch64                     4:2.97-3.fc28                                  fedora                      1.5 M
 perl-Errno                                       aarch64                     1.28-418.fc28                                  updates                      76 k
 perl-Error                                       noarch                      1:0.17025-2.fc28                               fedora                       45 k
 perl-Exporter                                    noarch                      5.72-396.fc28                                  fedora                       33 k
 perl-File-Path                                   noarch                      2.16-1.fc28                                    updates                      37 k
 perl-File-Temp                                   noarch                      0.230.600-1.fc28                               updates                      62 k
 perl-Getopt-Long                                 noarch                      1:2.50-4.fc28                                  fedora                       62 k
 perl-Git                                         noarch                      2.17.2-2.fc28                                  updates                      73 k
 perl-HTTP-Tiny                                   noarch                      0.076-1.fc28                                   updates                      57 k
 perl-IO                                          aarch64                     1.38-418.fc28                                  updates                     142 k
 perl-IO-Socket-IP                                noarch                      0.39-5.fc28                                    fedora                       46 k
 perl-MIME-Base64                                 aarch64                     3.15-397.fc28                                  updates                      30 k
 perl-Net-SSLeay                                  aarch64                     1.85-1.fc28                                    fedora                      351 k
 perl-PathTools                                   aarch64                     3.75-1.fc28                                    updates                      89 k
 perl-Pod-Escapes                                 noarch                      1:1.07-395.fc28                                fedora                       19 k
 perl-Pod-Perldoc                                 noarch                      3.28.01-1.fc28                                 updates                      87 k
 perl-Pod-Simple                                  noarch                      1:3.35-395.fc28                                fedora                      212 k
 perl-Pod-Usage                                   noarch                      4:1.69-395.fc28                                fedora                       33 k
 perl-Scalar-List-Utils                           aarch64                     3:1.49-2.fc28                                  fedora                       66 k
 perl-Socket                                      aarch64                     4:2.029-1.fc28                                 updates                      58 k
 perl-Storable                                    aarch64                     1:3.15-1.fc28                                  updates                      94 k
 perl-Term-ANSIColor                              noarch                      4.06-396.fc28                                  fedora                       45 k
 perl-Term-Cap                                    noarch                      1.17-395.fc28                                  fedora                       22 k
 perl-TermReadKey                                 aarch64                     2.37-7.fc28                                    fedora                       39 k
 perl-Text-ParseWords                             noarch                      3.30-395.fc28                                  fedora                       17 k
 perl-Text-Tabs+Wrap                              noarch                      2013.0523-395.fc28                             fedora                       23 k
 perl-Time-Local                                  noarch                      1:1.280-1.fc28                                 updates                      32 k
 perl-URI                                         noarch                      1.73-2.fc28                                    fedora                      115 k
 perl-Unicode-Normalize                           aarch64                     1.25-397.fc28                                  updates                      77 k
 perl-constant                                    noarch                      1.33-396.fc28                                  fedora                       24 k
 perl-interpreter                                 aarch64                     4:5.26.3-418.fc28                              updates                     6.3 M
 perl-libnet                                      noarch                      3.11-3.fc28                                    fedora                      120 k
 perl-libs                                        aarch64                     4:5.26.3-418.fc28                              updates                     1.5 M
 perl-macros                                      aarch64                     4:5.26.3-418.fc28                              updates                      72 k
 perl-parent                                      noarch                      1:0.236-395.fc28                               fedora                       19 k
 perl-podlators                                   noarch                      4.11-1.fc28                                    updates                     117 k
 perl-srpm-macros                                 noarch                      1-25.fc28                                      fedora                      9.7 k
 perl-threads                                     aarch64                     1:2.21-2.fc28                                  fedora                       59 k
 perl-threads-shared                              aarch64                     1.59-1.fc28                                    updates                      46 k
 python-srpm-macros                               noarch                      3-30.fc28                                      updates                      11 k
 python2                                          aarch64                     2.7.15-4.fc28                                  updates                     102 k
 python2-asn1crypto                               noarch                      0.24.0-1.fc28                                  fedora                      180 k
 python2-bcrypt                                   aarch64                     3.1.4-3.fc28                                   fedora                       39 k
 python2-catkin_pkg                               noarch                      0.4.5-1.fc28                                   updates                     323 k
 python2-cffi                                     aarch64                     1.11.5-3.fc28                                  updates                     233 k
 python2-cryptography                             aarch64                     2.3-1.fc28                                     updates                     485 k
 python2-dateutil                                 noarch                      1:2.6.1-3.fc28                                 fedora                      250 k
 python2-docutils                                 noarch                      0.14-3.fc28                                    fedora                      1.6 M
 python2-enum34                                   noarch                      1.1.6-4.fc28                                   fedora                       57 k
 python2-idna                                     noarch                      2.5-4.fc28                                     fedora                       99 k
 python2-ipaddress                                noarch                      1.0.18-4.fc28                                  fedora                       39 k
 python2-libs                                     aarch64                     2.7.15-4.fc28                                  updates                     5.9 M
 python2-paramiko                                 noarch                      2.4.2-1.fc28                                   updates                     290 k
 python2-pip                                      noarch                      9.0.3-2.fc28                                   updates                     2.0 M
 python2-ply                                      noarch                      3.9-6.fc28                                     fedora                      107 k
 python2-pyasn1                                   noarch                      0.3.7-2.fc28                                   fedora                      122 k
 python2-pycparser                                noarch                      2.14-13.fc28                                   fedora                      146 k
 python2-pycurl                                   aarch64                     7.43.0.2-1.fc28                                updates                     226 k
 python2-pynacl                                   aarch64                     1.2.0-2.fc28                                   fedora                       77 k
 python2-pyyaml                                   aarch64                     5.1-1.fc28                                     updates                     178 k
 python2-rosdistro                                noarch                      0.6.2-3.fc28                                   fedora                      247 k
 python2-rospkg                                   noarch                      1.1.0-3.fc28                                   fedora                      290 k
 python2-setuptools                               noarch                      40.8.0-1.fc28                                  updates                     661 k
 python2-six                                      noarch                      1.11.0-3.fc28                                  fedora                       37 k
 python2-vcstools                                 noarch                      0.1.39-3.fc26                                  fedora                      244 k
 python3-catkin_pkg                               noarch                      0.4.5-1.fc28                                   updates                     324 k
 python3-dateutil                                 noarch                      1:2.6.1-3.fc28                                 fedora                      250 k
 python3-docutils                                 noarch                      0.14-3.fc28                                    fedora                      1.6 M
 python3-pyyaml                                   aarch64                     5.1-1.fc28                                     updates                     178 k
 python3-rosdistro                                noarch                      0.6.2-3.fc28                                   fedora                      247 k
 python3-rospkg                                   noarch                      1.1.0-3.fc28                                   fedora                      290 k
 qt5-srpm-macros                                  noarch                      5.11.3-1.fc28                                  updates                      10 k
 rust-srpm-macros                                 noarch                      6-3.fc28                                       updates                     8.9 k
 subversion                                       aarch64                     1.11.1-1.fc28                                  updates                     1.1 M
 subversion-libs                                  aarch64                     1.11.1-1.fc28                                  updates                     1.5 M
 utf8proc                                         aarch64                     2.1.1-2.fc28                                   updates                      64 k
 zip                                              aarch64                     3.0-21.fc28                                    fedora                      265 k
 zstd                                             aarch64                     1.4.0-1.fc28                                   updates                     293 k
Installing weak dependencies:
 apr-util-bdb                                     aarch64                     1.6.1-8.fc28                                   updates                      24 k
 apr-util-openssl                                 aarch64                     1.6.1-8.fc28                                   updates                      26 k
 perl-IO-Socket-SSL                               noarch                      2.066-1.fc28                                   updates                     298 k
 perl-Mozilla-CA                                  noarch                      20160104-7.fc28                                fedora                       14 k

Transaction Summary
===============================================================================================================================================================
Install  138 Packages
Upgrade    1 Package

Total download size: 75 M
Is this ok [y/N]: y
Downloading Packages:
(1/139): python3-rosinstall_generator-0.1.14-1.fc28.noarch.rpm                                                                 4.8 kB/s |  30 kB     00:06    
(2/139): python2-wstool-0.1.17-3 3.9 kB/s |  93 kB     00:24     (3/139): python2-rosinstall-0.7. 4.9 kB/s | 242 kB     00:49    9(4/139): which-2.21-8.fc28.aarch  15 kB/s |  47 kB     00:03    
(5/139): unzip-6.0-38.fc28.aarch  64 kB/s | 184 kB     00:02    
(6/139): cpio-2.12-7.fc28.aarch6 158 kB/s | 259 kB     00:01    
(7/139): bzip2-1.0.6-26.fc28.aar  69 kB/s |  58 kB     00:00    
(8/139): python2-rosdistro-0.6.2 179 kB/s | 247 kB     00:01    
(9/139): python2-rospkg-1.1.0-3. 300 kB/s | 290 kB     00:00    
(10/139): python3-rosdistro-0.6. 397 kB/s | 247 kB     00:00    
(11/139): python3-rospkg-1.1.0-3 470 kB/s | 290 kB     00:00    
(12/139): make-4.2.1-6.fc28.aarc  11 kB/s | 491 kB     00:44    
[MIRROR] python2-rosdep-0.11.4-9.fc28.noarch.rpm: Curl error (28): Timeout was reached for https://fedora-archive.ip-connect.info/fedora/linux/releases/28/Everything/aarch64/os/Packages/p/python2-rosdep-0.11.4-9.fc28.noarch.rpm [Operation too slow. Less than 1000 bytes/sec transferred the last 30 seconds]
(13/139): python2-rosdep-0.11.4- 4.2 kB/s | 307 kB     01:13    
(14/139): python2-dateutil-2.6.1 178 kB/s | 250 kB     00:01    
(15/139): python2-vcstools-0.1.3 170 kB/s | 244 kB     00:01    
(16/139): gc-7.6.4-3.fc28.aarch6 148 kB/s |  97 kB     00:00    
(17/139): mercurial-4.4.2-4.fc28 139 kB/s | 4.2 MB     00:31    
(18/139): guile-2.0.14-7.fc28.aa 132 kB/s | 3.5 MB     00:26    
(19/139): libatomic_ops-7.6.2-3.  61 kB/s |  36 kB     00:00    
(20/139): bzr-2.7.0-20.fc28.aarc 146 kB/s | 6.4 MB     00:44    
(21/139): libstdc++-devel-8.3.1- 300 kB/s | 2.1 MB     00:07    
(22/139): redhat-rpm-config-110- 123 kB/s |  78 kB     00:00    
(23/139): dwz-0.12-7.fc28.aarch6 282 kB/s | 102 kB     00:00    
(24/139): fpc-srpm-macros-1.1-4.  24 kB/s | 7.5 kB     00:00    
(25/139): ghc-srpm-macros-1.4.2-  21 kB/s | 8.2 kB     00:00    
(26/139): gnat-srpm-macros-4-5.f  23 kB/s | 8.8 kB     00:00    
(27/139): go-srpm-macros-2-16.fc  30 kB/s |  13 kB     00:00    
(28/139): nim-srpm-macros-1-1.fc  21 kB/s | 7.6 kB     00:00    
(29/139): ocaml-srpm-macros-5-2.  22 kB/s | 7.8 kB     00:00    
(30/139): openblas-srpm-macros-2  21 kB/s | 6.6 kB     00:00    
(31/139): perl-srpm-macros-1-25.  24 kB/s | 9.7 kB     00:00    
(32/139): zip-3.0-21.fc28.aarch6 446 kB/s | 265 kB     00:00    
(33/139): findutils-4.6.0-19.fc2 375 kB/s | 523 kB     00:01    
(34/139): rpm-build-4.14.2.1-2.f  82 kB/s | 161 kB     00:01    
(35/139): patch-2.7.6-8.fc28.aar 214 kB/s | 133 kB     00:00    
(36/139): xz-5.2.4-2.fc28.aarch6 238 kB/s | 148 kB     00:00    
(37/139): file-5.33-10.fc28.aarc 208 kB/s |  75 kB     00:00    
[MIRROR] python2-six-1.11.0-3.fc28.noarch.rpm: Curl error (28): Timeout was reached for http://fedora-archive.ip-connect.info/fedora/linux/releases/28/Everything/aarch64/os/Packages/p/python2-six-1.11.0-3.fc28.noarch.rpm [Connection timed out after 30002 milliseconds]
(38/139): elfutils-0.174-5.fc28. 292 kB/s | 322 kB     00:01    
(39/139): python2-six-1.11.0-3.f 1.2 kB/s |  37 kB     00:30    
(40/139): python3-pyyaml-5.1-1.f 110 kB/s | 178 kB     00:01    
(41/139): python3-dateutil-2.6.1 187 kB/s | 250 kB     00:01    
(42/139): python3-catkin_pkg-0.4  86 kB/s | 324 kB     00:03    
(43/139): git-2.17.2-2.fc28.aarc 114 kB/s | 221 kB     00:01    
(44/139): python3-docutils-0.14- 176 kB/s | 1.6 MB     00:09    
(45/139): git-core-doc-2.17.2-2.  53 kB/s | 2.3 MB     00:45    
(46/139): perl-Git-2.17.2-2.fc28  88 kB/s |  73 kB     00:00    
(47/139): libsecret-0.18.6-1.fc2 110 kB/s | 158 kB     00:01    
(48/139): perl-Getopt-Long-2.50-  28 kB/s |  62 kB     00:02    
(49/139): perl-TermReadKey-2.37-  19 kB/s |  39 kB     00:02    
(50/139): less-530-1.fc28.aarch6  66 kB/s | 160 kB     00:02    
(51/139): perl-libs-5.26.3-418.f  98 kB/s | 1.5 MB     00:15    
(52/139): perl-Error-0.17025-2.f  71 kB/s |  45 kB     00:00    
(53/139): perl-Exporter-5.72-396  57 kB/s |  33 kB     00:00    
(54/139): perl-Pod-Usage-1.69-39  60 kB/s |  33 kB     00:00    
(55/139): perl-Text-ParseWords-3  52 kB/s |  17 kB     00:00    
(56/139): perl-constant-1.33-396  44 kB/s |  24 kB     00:00    
(57/139): perl-Scalar-List-Utils  79 kB/s |  66 kB     00:00    
(58/139): git-core-2.17.2-2.fc28  49 kB/s | 4.0 MB     01:23    
(59/139): perl-Text-Tabs+Wrap-20 7.6 kB/s |  23 kB     00:03    
(60/139): perl-parent-0.236-395. 6.1 kB/s |  19 kB     00:03    
(61/139): gcc-c++-8.3.1-2.fc28.a  83 kB/s |  10 MB     02:07    
(62/139): perl-threads-2.21-2.fc 6.9 kB/s |  59 kB     00:08    
(63/139): python2-2.7.15-4.fc28.  11 kB/s | 102 kB     00:09    
(64/139): gdbm-1.14.1-4.fc28.aar  61 kB/s | 119 kB     00:01    
(65/139): python2-pyyaml-5.1-1.f  58 kB/s | 178 kB     00:03    
(66/139): subversion-1.11.1-1.fc  77 kB/s | 1.1 MB     00:14    
(67/139): subversion-libs-1.11.1 132 kB/s | 1.5 MB     00:11    
(68/139): apr-1.6.3-5.fc28.aarch 131 kB/s | 119 kB     00:00    
(69/139): libserf-1.3.9-6.fc28.a 170 kB/s |  57 kB     00:00    
(70/139): python2-catkin_pkg-0.4 138 kB/s | 323 kB     00:02    
(71/139): python2-libs-2.7.15-4. 120 kB/s | 5.9 MB     00:50    
(72/139): python2-docutils-0.14- 119 kB/s | 1.6 MB     00:13    
(73/139): perl-Carp-1.42-396.fc2  76 kB/s |  29 kB     00:00    
(74/139): perl-PathTools-3.75-1.  96 kB/s |  89 kB     00:00    
(75/139): perl-interpreter-5.26.  84 kB/s | 6.3 MB     01:16    
(76/139): python2-setuptools-40.  89 kB/s | 661 kB     00:07    
(77/139): perl-Errno-1.28-418.fc  34 kB/s |  76 kB     00:02    
(78/139): perl-Pod-Perldoc-3.28. 103 kB/s |  87 kB     00:00    
(79/139): groff-base-1.22.3-15.f 138 kB/s | 979 kB     00:07    
(80/139): perl-Pod-Escapes-1.07-  19 kB/s |  19 kB     00:01    
(81/139): perl-podlators-4.11-1.  64 kB/s | 117 kB     00:01    
(82/139): perl-Term-ANSIColor-4.  42 kB/s |  45 kB     00:01    
(83/139): perl-Term-Cap-1.17-395  60 kB/s |  22 kB     00:00    
(84/139): perl-MIME-Base64-3.15-  67 kB/s |  30 kB     00:00    
(85/139): perl-Pod-Simple-3.35-3  18 kB/s | 212 kB     00:11    
(86/139): perl-Storable-3.15-1.f 102 kB/s |  94 kB     00:00    
(87/139): emacs-filesystem-26.1-  79 kB/s |  68 kB     00:00    
(88/139): apr-util-1.6.1-8.fc28. 102 kB/s | 103 kB     00:01    
(89/139): libtool-ltdl-2.4.6-24.  93 kB/s |  56 kB     00:00    
(90/139): python2-paramiko-2.4.2 127 kB/s | 290 kB     00:02    
(91/139): python2-bcrypt-3.1.4-3  18 kB/s |  39 kB     00:02    
(92/139): python2-pyasn1-0.3.7-2  82 kB/s | 122 kB     00:01    
(93/139): python2-pynacl-1.2.0-2  45 kB/s |  77 kB     00:01    
(94/139): perl-Encode-2.97-3.fc2  85 kB/s | 1.5 MB     00:17    
(95/139): utf8proc-2.1.1-2.fc28.  63 kB/s |  64 kB     00:01    
(96/139): python2-pycurl-7.43.0. 119 kB/s | 226 kB     00:01    
(97/139): libbabeltrace-1.5.4-2.  94 kB/s | 187 kB     00:01    
(98/139): zstd-1.4.0-1.fc28.aarc 147 kB/s | 293 kB     00:01    
(99/139): annobin-5.7-3.fc28.aar  93 kB/s |  86 kB     00:00    
(100/139): python-srpm-macros-3-  33 kB/s |  11 kB     00:00    
(101/139): qt5-srpm-macros-5.11.  23 kB/s |  10 kB     00:00    
(102/139): rust-srpm-macros-6-3.  17 kB/s | 8.9 kB     00:00    
(103/139): python2-asn1crypto-0.  62 kB/s | 180 kB     00:02    
(104/139): python2-enum34-1.1.6-  68 kB/s |  57 kB     00:00    
(105/139): python2-cryptography- 101 kB/s | 485 kB     00:04    
(106/139): python2-ipaddress-1.0  14 kB/s |  39 kB     00:02    
(107/139): python2-idna-2.5-4.fc  23 kB/s |  99 kB     00:04    
(108/139): python2-cffi-1.11.5-3 103 kB/s | 233 kB     00:02    
(109/139): python2-pycparser-2.1  63 kB/s | 146 kB     00:02    
(110/139): python2-ply-3.9-6.fc2  69 kB/s | 107 kB     00:01    
(111/139): perl-File-Path-2.16-1  46 kB/s |  37 kB     00:00    
(112/139): perl-IO-1.38-418.fc28 107 kB/s | 142 kB     00:01    
(113/139): gdb-headless-8.1.1-4. 171 kB/s | 3.0 MB     00:18    
(114/139): perl-Unicode-Normaliz  37 kB/s |  77 kB     00:02    
(115/139): perl-macros-5.26.3-41  36 kB/s |  72 kB     00:01    
(116/139): perl-threads-shared-1  78 kB/s |  46 kB     00:00    
(117/139): perl-File-Temp-0.230. 100 kB/s |  62 kB     00:00    
(118/139): perl-HTTP-Tiny-0.076- 104 kB/s |  57 kB     00:00    
(119/139): perl-Socket-2.029-1.f  69 kB/s |  58 kB     00:00    
(120/139): python2-pip-9.0.3-2.f 265 kB/s | 2.0 MB     00:07    
(121/139): perl-Time-Local-1.280  17 kB/s |  32 kB     00:01    
(122/139): fipscheck-lib-1.5.0-4  47 kB/s |  14 kB     00:00    
(123/139): openssh-7.8p1-4.fc28. 204 kB/s | 469 kB     00:02    
(124/139): fipscheck-1.5.0-4.fc2  41 kB/s |  26 kB     00:00    
(125/139): libedit-3.1-23.201703  42 kB/s |  97 kB     00:02    
(126/139): openssh-clients-7.8p1 145 kB/s | 608 kB     00:04    
(127/139): apr-util-bdb-1.6.1-8.  62 kB/s |  24 kB     00:00    
(128/139): apr-util-openssl-1.6.  75 kB/s |  26 kB     00:00    
(129/139): perl-Mozilla-CA-20160  34 kB/s |  14 kB     00:00    
(130/139): libsodium-1.0.17-1.fc  95 kB/s | 113 kB     00:01    
(131/139): perl-IO-Socket-IP-0.3  55 kB/s |  46 kB     00:00    
(132/139): perl-URI-1.73-2.fc28.  39 kB/s | 115 kB     00:02    
(133/139): perl-IO-Socket-SSL-2.  70 kB/s | 298 kB     00:04    
(134/139): perl-Data-Dumper-2.16  60 kB/s |  56 kB     00:00    
(135/139): perl-libnet-3.11-3.fc 101 kB/s | 120 kB     00:01    
(136/139): perl-Digest-MD5-2.55-  61 kB/s |  36 kB     00:00    
(137/139): perl-Digest-1.17-395.  44 kB/s |  26 kB     00:00    
(138/139): perl-Net-SSLeay-1.85-  57 kB/s | 351 kB     00:06    
(139/139): file-libs-5.33-10.fc2 114 kB/s | 539 kB     00:04    
-----------------------------------------------------------------
Total                            220 kB/s |  75 MB     05:51     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                         1/1 
  Installing       : perl-Exporter-5.72-396.fc28.noarc     1/140 
  Installing       : perl-Carp-1.42-396.fc28.noarch        2/140 
  Installing       : perl-libs-4:5.26.3-418.fc28.aarch     3/140 
  Installing       : perl-Scalar-List-Utils-3:1.49-2.f     4/140 
  Upgrading        : file-libs-5.33-10.fc28.aarch64        5/140 
  Installing       : apr-1.6.3-5.fc28.aarch64              6/140 
  Running scriptlet: apr-1.6.3-5.fc28.aarch64              6/140 
  Installing       : apr-util-bdb-1.6.1-8.fc28.aarch64     7/140 
  Installing       : apr-util-openssl-1.6.1-8.fc28.aar     8/140 
  Installing       : apr-util-1.6.1-8.fc28.aarch64         9/140 
  Running scriptlet: apr-util-1.6.1-8.fc28.aarch64         9/140 
  Installing       : fipscheck-1.5.0-4.fc28.aarch64       10/140 
  Installing       : fipscheck-lib-1.5.0-4.fc28.aarch6    11/140 
  Running scriptlet: fipscheck-lib-1.5.0-4.fc28.aarch6    11/140 
  Installing       : python3-pyyaml-5.1-1.fc28.aarch64    12/140 
  Installing       : python3-rospkg-1.1.0-3.fc28.noarc    13/140 
  Installing       : libserf-1.3.9-6.fc28.aarch64         14/140 
  Running scriptlet: libserf-1.3.9-6.fc28.aarch64         14/140 
  Installing       : file-5.33-10.fc28.aarch64            15/140 
  Installing       : perl-Text-ParseWords-3.30-395.fc2    16/140 
  Installing       : utf8proc-2.1.1-2.fc28.aarch64        17/140 
  Running scriptlet: utf8proc-2.1.1-2.fc28.aarch64        17/140 
  Installing       : emacs-filesystem-1:26.1-3.fc28.no    18/140 
  Installing       : findutils-1:4.6.0-19.fc28.aarch64    19/140 
  Running scriptlet: findutils-1:4.6.0-19.fc28.aarch64    19/140 
  Installing       : unzip-6.0-38.fc28.aarch64            20/140 
  Installing       : zip-3.0-21.fc28.aarch64              21/140 
  Installing       : subversion-libs-1.11.1-1.fc28.aar    22/140 
  Running scriptlet: subversion-libs-1.11.1-1.fc28.aar    22/140 
  Installing       : subversion-1.11.1-1.fc28.aarch64     23/140 
  Running scriptlet: subversion-1.11.1-1.fc28.aarch64     23/140 
  Running scriptlet: openssh-7.8p1-4.fc28.aarch64         24/140 
  Installing       : openssh-7.8p1-4.fc28.aarch64         24/140 
  Installing       : perl-Term-ANSIColor-4.06-396.fc28    25/140 
  Installing       : perl-macros-4:5.26.3-418.fc28.aar    26/140 
  Installing       : perl-constant-1.33-396.fc28.noarc    27/140 
  Installing       : perl-Text-Tabs+Wrap-2013.0523-395    28/140 
  Installing       : perl-parent-1:0.236-395.fc28.noar    29/140 
  Installing       : perl-threads-1:2.21-2.fc28.aarch6    30/140 
  Installing       : perl-PathTools-3.75-1.fc28.aarch6    31/140 
  Installing       : perl-Errno-1.28-418.fc28.aarch64     32/140 
  Installing       : perl-File-Path-2.16-1.fc28.noarch    33/140 
  Installing       : perl-Unicode-Normalize-1.25-397.f    34/140 
  Installing       : perl-threads-shared-1.59-1.fc28.a    35/140 
  Installing       : perl-interpreter-4:5.26.3-418.fc2    36/140 
  Installing       : perl-Socket-4:2.029-1.fc28.aarch6    37/140 
  Installing       : perl-IO-1.38-418.fc28.aarch64        38/140 
  Installing       : perl-MIME-Base64-3.15-397.fc28.aa    39/140 
  Installing       : perl-IO-Socket-IP-0.39-5.fc28.noa    40/140 
  Installing       : perl-Time-Local-1:1.280-1.fc28.no    41/140 
  Installing       : perl-File-Temp-0.230.600-1.fc28.n    42/140 
  Installing       : perl-Net-SSLeay-1.85-1.fc28.aarch    43/140 
  Installing       : perl-Digest-1.17-395.fc28.noarch     44/140 
  Installing       : perl-Digest-MD5-2.55-397.fc28.aar    45/140 
  Installing       : perl-Storable-1:3.15-1.fc28.aarch    46/140 
  Installing       : perl-TermReadKey-2.37-7.fc28.aarc    47/140 
  Installing       : perl-Error-1:0.17025-2.fc28.noarc    48/140 
  Installing       : perl-Pod-Escapes-1:1.07-395.fc28.    49/140 
  Installing       : perl-Term-Cap-1.17-395.fc28.noarc    50/140 
  Installing       : perl-Data-Dumper-2.167-399.fc28.a    51/140 
  Installing       : perl-Mozilla-CA-20160104-7.fc28.n    52/140 
  Installing       : libsodium-1.0.17-1.fc28.aarch64      53/140 
  Installing       : libedit-3.1-23.20170329cvs.fc28.a    54/140 
  Installing       : openssh-clients-7.8p1-4.fc28.aarc    55/140 
  Installing       : rust-srpm-macros-6-3.fc28.noarch     56/140 
  Installing       : qt5-srpm-macros-5.11.3-1.fc28.noa    57/140 
  Installing       : python-srpm-macros-3-30.fc28.noar    58/140 
  Installing       : annobin-5.7-3.fc28.aarch64           59/140 
  Running scriptlet: annobin-5.7-3.fc28.aarch64           59/140 
  Installing       : zstd-1.4.0-1.fc28.aarch64            60/140 
  Installing       : libbabeltrace-1.5.4-2.fc28.aarch6    61/140 
  Running scriptlet: libbabeltrace-1.5.4-2.fc28.aarch6    61/140 
  Installing       : libtool-ltdl-2.4.6-24.fc28.aarch6    62/140 
  Running scriptlet: libtool-ltdl-2.4.6-24.fc28.aarch6    62/140 
  Installing       : groff-base-1.22.3-15.fc28.aarch64    63/140 
  Installing       : perl-HTTP-Tiny-0.076-1.fc28.noarc    64/140 
  Installing       : perl-libnet-3.11-3.fc28.noarch       65/140 
  Installing       : perl-IO-Socket-SSL-2.066-1.fc28.n    66/140 
  Installing       : perl-URI-1.73-2.fc28.noarch          67/140 
  Installing       : perl-Encode-4:2.97-3.fc28.aarch64    68/140 
  Installing       : perl-Pod-Simple-1:3.35-395.fc28.n    69/140 
  Installing       : perl-Pod-Usage-4:1.69-395.fc28.no    70/140 
  Installing       : perl-Getopt-Long-1:2.50-4.fc28.no    71/140 
  Installing       : perl-podlators-4.11-1.fc28.noarch    72/140 
  Installing       : perl-Pod-Perldoc-3.28.01-1.fc28.n    73/140 
  Installing       : gdbm-1:1.14.1-4.fc28.aarch64         74/140 
  Installing       : python2-libs-2.7.15-4.fc28.aarch6    75/140 
  Installing       : python2-setuptools-40.8.0-1.fc28.    76/140 
  Installing       : python2-pip-9.0.3-2.fc28.noarch      77/140 
  Installing       : python2-2.7.15-4.fc28.aarch64        78/140 
  Installing       : python2-pyyaml-5.1-1.fc28.aarch64    79/140 
  Installing       : python2-six-1.11.0-3.fc28.noarch     80/140 
  Installing       : python2-dateutil-1:2.6.1-3.fc28.n    81/140 
  Installing       : python2-rospkg-1.1.0-3.fc28.noarc    82/140 
  Installing       : mercurial-4.4.2-4.fc28.aarch64       83/140 
  Installing       : python2-docutils-0.14-3.fc28.noar    84/140 
  Installing       : python2-catkin_pkg-0.4.5-1.fc28.n    85/140 
  Installing       : python2-rosdistro-0.6.2-3.fc28.no    86/140 
  Installing       : python2-pyasn1-0.3.7-2.fc28.noarc    87/140 
  Installing       : python2-pycurl-7.43.0.2-1.fc28.aa    88/140 
  Installing       : python2-asn1crypto-0.24.0-1.fc28.    89/140 
  Installing       : python2-enum34-1.1.6-4.fc28.noarc    90/140 
  Installing       : python2-idna-2.5-4.fc28.noarch       91/140 
  Installing       : python2-ipaddress-1.0.18-4.fc28.n    92/140 
  Installing       : python2-ply-3.9-6.fc28.noarch        93/140 
  Installing       : python2-pycparser-2.14-13.fc28.no    94/140 
  Installing       : python2-cffi-1.11.5-3.fc28.aarch6    95/140 
  Installing       : python2-bcrypt-3.1.4-3.fc28.aarch    96/140 
  Installing       : python2-pynacl-1.2.0-2.fc28.aarch    97/140 
  Installing       : python2-cryptography-2.3-1.fc28.a    98/140 
  Installing       : python2-paramiko-2.4.2-1.fc28.noa    99/140 
  Installing       : bzr-2.7.0-20.fc28.aarch64           100/140 
  Installing       : less-530-1.fc28.aarch64             101/140 
  Installing       : git-core-2.17.2-2.fc28.aarch64      102/140 
  Installing       : git-core-doc-2.17.2-2.fc28.noarch   103/140 
  Installing       : libsecret-0.18.6-1.fc28.aarch64     104/140 
  Installing       : perl-Git-2.17.2-2.fc28.noarch       105/140 
  Installing       : git-2.17.2-2.fc28.aarch64           106/140 
  Installing       : python2-vcstools-0.1.39-3.fc26.no   107/140 
  Installing       : python2-rosinstall-0.7.7-9.fc28.n   108/140 
  Installing       : python2-wstool-0.1.17-3.fc28.noar   109/140 
  Installing       : python3-docutils-0.14-3.fc28.noar   110/140 
  Installing       : python3-dateutil-1:2.6.1-3.fc28.n   111/140 
  Installing       : python3-catkin_pkg-0.4.5-1.fc28.n   112/140 
  Installing       : python3-rosdistro-0.6.2-3.fc28.no   113/140 
  Installing       : elfutils-0.174-5.fc28.aarch64       114/140 
  Installing       : xz-5.2.4-2.fc28.aarch64             115/140 
  Installing       : patch-2.7.6-8.fc28.aarch64          116/140 
  Installing       : perl-srpm-macros-1-25.fc28.noarch   117/140 
  Installing       : openblas-srpm-macros-2-2.fc27.noa   118/140 
  Installing       : ocaml-srpm-macros-5-2.fc27.noarch   119/140 
  Installing       : nim-srpm-macros-1-1.fc28.noarch     120/140 
  Installing       : go-srpm-macros-2-16.fc28.noarch     121/140 
  Installing       : gnat-srpm-macros-4-5.fc28.noarch    122/140 
  Installing       : ghc-srpm-macros-1.4.2-7.fc28.noar   123/140 
  Installing       : fpc-srpm-macros-1.1-4.fc28.noarch   124/140 
  Installing       : dwz-0.12-7.fc28.aarch64             125/140 
  Installing       : redhat-rpm-config-110-1.fc28.noar   126/140 
  Installing       : libstdc++-devel-8.3.1-2.fc28.aarc   127/140 
  Installing       : libatomic_ops-7.6.2-3.fc28.aarch6   128/140 
  Installing       : gc-7.6.4-3.fc28.aarch64             129/140 
  Installing       : guile-5:2.0.14-7.fc28.aarch64       130/140 
  Running scriptlet: guile-5:2.0.14-7.fc28.aarch64       130/140 
  Installing       : gdb-headless-8.1.1-4.fc28.aarch64   131/140 
  Installing       : bzip2-1.0.6-26.fc28.aarch64         132/140 
  Installing       : cpio-2.12-7.fc28.aarch64            133/140 
  Running scriptlet: cpio-2.12-7.fc28.aarch64            133/140 
  Installing       : rpm-build-4.14.2.1-2.fc28.aarch64   134/140 
  Installing       : make-1:4.2.1-6.fc28.aarch64         135/140 
  Running scriptlet: make-1:4.2.1-6.fc28.aarch64         135/140 
  Installing       : gcc-c++-8.3.1-2.fc28.aarch64        136/140 
  Installing       : python3-rosinstall_generator-0.1.   137/140 
  Installing       : python2-rosdep-0.11.4-9.fc28.noar   138/140 
  Installing       : which-2.21-8.fc28.aarch64           139/140 
  Running scriptlet: which-2.21-8.fc28.aarch64           139/140 
install-info: No such file or directory for /usr/share/info/which.info.gz
  Cleanup          : file-libs-5.33-9.fc28.aarch64       140/140 
  Running scriptlet: guile-5:2.0.14-7.fc28.aarch64       140/140 
  Running scriptlet: file-libs-5.33-9.fc28.aarch64       140/140 
  Verifying        : python2-rosdep-0.11.4-9.fc28.noar     1/140 
  Verifying        : python3-rosinstall_generator-0.1.     2/140 
  Verifying        : python2-wstool-0.1.17-3.fc28.noar     3/140 
  Verifying        : python2-rosinstall-0.7.7-9.fc28.n     4/140 
  Verifying        : make-1:4.2.1-6.fc28.aarch64           5/140 
  Verifying        : which-2.21-8.fc28.aarch64             6/140 
  Verifying        : unzip-6.0-38.fc28.aarch64             7/140 
  Verifying        : cpio-2.12-7.fc28.aarch64              8/140 
  Verifying        : bzip2-1.0.6-26.fc28.aarch64           9/140 
  Verifying        : python2-rosdistro-0.6.2-3.fc28.no    10/140 
  Verifying        : python2-rospkg-1.1.0-3.fc28.noarc    11/140 
  Verifying        : python3-rosdistro-0.6.2-3.fc28.no    12/140 
  Verifying        : python3-rospkg-1.1.0-3.fc28.noarc    13/140 
  Verifying        : bzr-2.7.0-20.fc28.aarch64            14/140 
  Verifying        : mercurial-4.4.2-4.fc28.aarch64       15/140 
  Verifying        : python2-dateutil-1:2.6.1-3.fc28.n    16/140 
  Verifying        : python2-vcstools-0.1.39-3.fc26.no    17/140 
  Verifying        : gc-7.6.4-3.fc28.aarch64              18/140 
  Verifying        : guile-5:2.0.14-7.fc28.aarch64        19/140 
  Verifying        : python2-six-1.11.0-3.fc28.noarch     20/140 
  Verifying        : libatomic_ops-7.6.2-3.fc28.aarch6    21/140 
  Verifying        : gcc-c++-8.3.1-2.fc28.aarch64         22/140 
  Verifying        : libstdc++-devel-8.3.1-2.fc28.aarc    23/140 
  Verifying        : redhat-rpm-config-110-1.fc28.noar    24/140 
  Verifying        : dwz-0.12-7.fc28.aarch64              25/140 
  Verifying        : fpc-srpm-macros-1.1-4.fc28.noarch    26/140 
  Verifying        : ghc-srpm-macros-1.4.2-7.fc28.noar    27/140 
  Verifying        : gnat-srpm-macros-4-5.fc28.noarch     28/140 
  Verifying        : go-srpm-macros-2-16.fc28.noarch      29/140 
  Verifying        : nim-srpm-macros-1-1.fc28.noarch      30/140 
  Verifying        : ocaml-srpm-macros-5-2.fc27.noarch    31/140 
  Verifying        : openblas-srpm-macros-2-2.fc27.noa    32/140 
  Verifying        : perl-srpm-macros-1-25.fc28.noarch    33/140 
  Verifying        : zip-3.0-21.fc28.aarch64              34/140 
  Verifying        : findutils-1:4.6.0-19.fc28.aarch64    35/140 
  Verifying        : rpm-build-4.14.2.1-2.fc28.aarch64    36/140 
  Verifying        : patch-2.7.6-8.fc28.aarch64           37/140 
  Verifying        : xz-5.2.4-2.fc28.aarch64              38/140 
  Verifying        : file-5.33-10.fc28.aarch64            39/140 
  Verifying        : elfutils-0.174-5.fc28.aarch64        40/140 
  Verifying        : python3-pyyaml-5.1-1.fc28.aarch64    41/140 
  Verifying        : python3-catkin_pkg-0.4.5-1.fc28.n    42/140 
  Verifying        : python3-dateutil-1:2.6.1-3.fc28.n    43/140 
  Verifying        : python3-docutils-0.14-3.fc28.noar    44/140 
  Verifying        : git-2.17.2-2.fc28.aarch64            45/140 
  Verifying        : git-core-2.17.2-2.fc28.aarch64       46/140 
  Verifying        : git-core-doc-2.17.2-2.fc28.noarch    47/140 
  Verifying        : perl-Git-2.17.2-2.fc28.noarch        48/140 
  Verifying        : libsecret-0.18.6-1.fc28.aarch64      49/140 
  Verifying        : perl-Getopt-Long-1:2.50-4.fc28.no    50/140 
  Verifying        : perl-TermReadKey-2.37-7.fc28.aarc    51/140 
  Verifying        : less-530-1.fc28.aarch64              52/140 
  Verifying        : perl-libs-4:5.26.3-418.fc28.aarch    53/140 
  Verifying        : perl-Error-1:0.17025-2.fc28.noarc    54/140 
  Verifying        : perl-Exporter-5.72-396.fc28.noarc    55/140 
  Verifying        : perl-Pod-Usage-4:1.69-395.fc28.no    56/140 
  Verifying        : perl-Text-ParseWords-3.30-395.fc2    57/140 
  Verifying        : perl-constant-1.33-396.fc28.noarc    58/140 
  Verifying        : perl-Scalar-List-Utils-3:1.49-2.f    59/140 
  Verifying        : perl-interpreter-4:5.26.3-418.fc2    60/140 
  Verifying        : perl-Text-Tabs+Wrap-2013.0523-395    61/140 
  Verifying        : perl-parent-1:0.236-395.fc28.noar    62/140 
  Verifying        : perl-threads-1:2.21-2.fc28.aarch6    63/140 
  Verifying        : python2-2.7.15-4.fc28.aarch64        64/140 
  Verifying        : python2-libs-2.7.15-4.fc28.aarch6    65/140 
  Verifying        : gdbm-1:1.14.1-4.fc28.aarch64         66/140 
  Verifying        : python2-pyyaml-5.1-1.fc28.aarch64    67/140 
  Verifying        : subversion-1.11.1-1.fc28.aarch64     68/140 
  Verifying        : subversion-libs-1.11.1-1.fc28.aar    69/140 
  Verifying        : apr-1.6.3-5.fc28.aarch64             70/140 
  Verifying        : libserf-1.3.9-6.fc28.aarch64         71/140 
  Verifying        : python2-catkin_pkg-0.4.5-1.fc28.n    72/140 
  Verifying        : python2-docutils-0.14-3.fc28.noar    73/140 
  Verifying        : python2-setuptools-40.8.0-1.fc28.    74/140 
  Verifying        : perl-Carp-1.42-396.fc28.noarch       75/140 
  Verifying        : perl-PathTools-3.75-1.fc28.aarch6    76/140 
  Verifying        : perl-Errno-1.28-418.fc28.aarch64     77/140 
  Verifying        : perl-Pod-Perldoc-3.28.01-1.fc28.n    78/140 
  Verifying        : groff-base-1.22.3-15.fc28.aarch64    79/140 
  Verifying        : perl-Encode-4:2.97-3.fc28.aarch64    80/140 
  Verifying        : perl-Pod-Simple-1:3.35-395.fc28.n    81/140 
  Verifying        : perl-Pod-Escapes-1:1.07-395.fc28.    82/140 
  Verifying        : perl-podlators-4.11-1.fc28.noarch    83/140 
  Verifying        : perl-Term-ANSIColor-4.06-396.fc28    84/140 
  Verifying        : perl-Term-Cap-1.17-395.fc28.noarc    85/140 
  Verifying        : perl-MIME-Base64-3.15-397.fc28.aa    86/140 
  Verifying        : perl-Storable-1:3.15-1.fc28.aarch    87/140 
  Verifying        : emacs-filesystem-1:26.1-3.fc28.no    88/140 
  Verifying        : apr-util-1.6.1-8.fc28.aarch64        89/140 
  Verifying        : libtool-ltdl-2.4.6-24.fc28.aarch6    90/140 
  Verifying        : python2-paramiko-2.4.2-1.fc28.noa    91/140 
  Verifying        : python2-bcrypt-3.1.4-3.fc28.aarch    92/140 
  Verifying        : python2-pyasn1-0.3.7-2.fc28.noarc    93/140 
  Verifying        : python2-pynacl-1.2.0-2.fc28.aarch    94/140 
  Verifying        : python2-pycurl-7.43.0.2-1.fc28.aa    95/140 
  Verifying        : utf8proc-2.1.1-2.fc28.aarch64        96/140 
  Verifying        : gdb-headless-8.1.1-4.fc28.aarch64    97/140 
  Verifying        : libbabeltrace-1.5.4-2.fc28.aarch6    98/140 
  Verifying        : zstd-1.4.0-1.fc28.aarch64            99/140 
  Verifying        : annobin-5.7-3.fc28.aarch64          100/140 
  Verifying        : python-srpm-macros-3-30.fc28.noar   101/140 
  Verifying        : qt5-srpm-macros-5.11.3-1.fc28.noa   102/140 
  Verifying        : rust-srpm-macros-6-3.fc28.noarch    103/140 
  Verifying        : python2-cryptography-2.3-1.fc28.a   104/140 
  Verifying        : python2-asn1crypto-0.24.0-1.fc28.   105/140 
  Verifying        : python2-enum34-1.1.6-4.fc28.noarc   106/140 
  Verifying        : python2-idna-2.5-4.fc28.noarch      107/140 
  Verifying        : python2-ipaddress-1.0.18-4.fc28.n   108/140 
  Verifying        : python2-cffi-1.11.5-3.fc28.aarch6   109/140 
  Verifying        : python2-pycparser-2.14-13.fc28.no   110/140 
  Verifying        : python2-ply-3.9-6.fc28.noarch       111/140 
  Verifying        : python2-pip-9.0.3-2.fc28.noarch     112/140 
  Verifying        : perl-File-Path-2.16-1.fc28.noarch   113/140 
  Verifying        : perl-IO-1.38-418.fc28.aarch64       114/140 
  Verifying        : perl-Unicode-Normalize-1.25-397.f   115/140 
  Verifying        : perl-macros-4:5.26.3-418.fc28.aar   116/140 
  Verifying        : perl-threads-shared-1.59-1.fc28.a   117/140 
  Verifying        : perl-File-Temp-0.230.600-1.fc28.n   118/140 
  Verifying        : perl-HTTP-Tiny-0.076-1.fc28.noarc   119/140 
  Verifying        : perl-Socket-4:2.029-1.fc28.aarch6   120/140 
  Verifying        : perl-Time-Local-1:1.280-1.fc28.no   121/140 
  Verifying        : openssh-clients-7.8p1-4.fc28.aarc   122/140 
  Verifying        : openssh-7.8p1-4.fc28.aarch64        123/140 
  Verifying        : fipscheck-lib-1.5.0-4.fc28.aarch6   124/140 
  Verifying        : libedit-3.1-23.20170329cvs.fc28.a   125/140 
  Verifying        : fipscheck-1.5.0-4.fc28.aarch64      126/140 
  Verifying        : libsodium-1.0.17-1.fc28.aarch64     127/140 
  Verifying        : apr-util-bdb-1.6.1-8.fc28.aarch64   128/140 
  Verifying        : apr-util-openssl-1.6.1-8.fc28.aar   129/140 
  Verifying        : perl-Mozilla-CA-20160104-7.fc28.n   130/140 
  Verifying        : perl-IO-Socket-SSL-2.066-1.fc28.n   131/140 
  Verifying        : perl-IO-Socket-IP-0.39-5.fc28.noa   132/140 
  Verifying        : perl-Net-SSLeay-1.85-1.fc28.aarch   133/140 
  Verifying        : perl-URI-1.73-2.fc28.noarch         134/140 
  Verifying        : perl-Data-Dumper-2.167-399.fc28.a   135/140 
  Verifying        : perl-libnet-3.11-3.fc28.noarch      136/140 
  Verifying        : perl-Digest-MD5-2.55-397.fc28.aar   137/140 
  Verifying        : perl-Digest-1.17-395.fc28.noarch    138/140 
  Verifying        : file-libs-5.33-10.fc28.aarch64      139/140 
  Verifying        : file-libs-5.33-9.fc28.aarch64       140/140 

Installed:
  python2-rosdep.noarch 0.11.4-9.fc28                            
  python2-rosinstall.noarch 0.7.7-9.fc28                         
  python2-wstool.noarch 0.1.17-3.fc28                            
  python3-rosinstall_generator.noarch 0.1.14-1.fc28              
  bzip2.aarch64 1.0.6-26.fc28                                    
  cpio.aarch64 2.12-7.fc28                                       
  findutils.aarch64 1:4.6.0-19.fc28                              
  gcc-c++.aarch64 8.3.1-2.fc28                                   
  make.aarch64 1:4.2.1-6.fc28                                    
  patch.aarch64 2.7.6-8.fc28                                     
  redhat-rpm-config.noarch 110-1.fc28                            
  rpm-build.aarch64 4.14.2.1-2.fc28                              
  unzip.aarch64 6.0-38.fc28                                      
  which.aarch64 2.21-8.fc28                                      
  xz.aarch64 5.2.4-2.fc28                                        
  apr-util-bdb.aarch64 1.6.1-8.fc28                              
  apr-util-openssl.aarch64 1.6.1-8.fc28                          
  perl-IO-Socket-SSL.noarch 2.066-1.fc28                         
  perl-Mozilla-CA.noarch 20160104-7.fc28                         
  annobin.aarch64 5.7-3.fc28                                     
  apr.aarch64 1.6.3-5.fc28                                       
  apr-util.aarch64 1.6.1-8.fc28                                  
  bzr.aarch64 2.7.0-20.fc28                                      
  dwz.aarch64 0.12-7.fc28                                        
  elfutils.aarch64 0.174-5.fc28                                  
  emacs-filesystem.noarch 1:26.1-3.fc28                          
  file.aarch64 5.33-10.fc28                                      
  fipscheck.aarch64 1.5.0-4.fc28                                 
  fipscheck-lib.aarch64 1.5.0-4.fc28                             
  fpc-srpm-macros.noarch 1.1-4.fc28                              
  gc.aarch64 7.6.4-3.fc28                                        
  gdb-headless.aarch64 8.1.1-4.fc28                              
  gdbm.aarch64 1:1.14.1-4.fc28                                   
  ghc-srpm-macros.noarch 1.4.2-7.fc28                            
  git.aarch64 2.17.2-2.fc28                                      
  git-core.aarch64 2.17.2-2.fc28                                 
  git-core-doc.noarch 2.17.2-2.fc28                              
  gnat-srpm-macros.noarch 4-5.fc28                               
  go-srpm-macros.noarch 2-16.fc28                                
  groff-base.aarch64 1.22.3-15.fc28                              
  guile.aarch64 5:2.0.14-7.fc28                                  
  less.aarch64 530-1.fc28                                        
  libatomic_ops.aarch64 7.6.2-3.fc28                             
  libbabeltrace.aarch64 1.5.4-2.fc28                             
  libedit.aarch64 3.1-23.20170329cvs.fc28                        
  libsecret.aarch64 0.18.6-1.fc28                                
  libserf.aarch64 1.3.9-6.fc28                                   
  libsodium.aarch64 1.0.17-1.fc28                                
  libstdc++-devel.aarch64 8.3.1-2.fc28                           
  libtool-ltdl.aarch64 2.4.6-24.fc28                             
  mercurial.aarch64 4.4.2-4.fc28                                 
  nim-srpm-macros.noarch 1-1.fc28                                
  ocaml-srpm-macros.noarch 5-2.fc27                              
  openblas-srpm-macros.noarch 2-2.fc27                           
  openssh.aarch64 7.8p1-4.fc28                                   
  openssh-clients.aarch64 7.8p1-4.fc28                           
  perl-Carp.noarch 1.42-396.fc28                                 
  perl-Data-Dumper.aarch64 2.167-399.fc28                        
  perl-Digest.noarch 1.17-395.fc28                               
  perl-Digest-MD5.aarch64 2.55-397.fc28                          
  perl-Encode.aarch64 4:2.97-3.fc28                              
  perl-Errno.aarch64 1.28-418.fc28                               
  perl-Error.noarch 1:0.17025-2.fc28                             
  perl-Exporter.noarch 5.72-396.fc28                             
  perl-File-Path.noarch 2.16-1.fc28                              
  perl-File-Temp.noarch 0.230.600-1.fc28                         
  perl-Getopt-Long.noarch 1:2.50-4.fc28                          
  perl-Git.noarch 2.17.2-2.fc28                                  
  perl-HTTP-Tiny.noarch 0.076-1.fc28                             
  perl-IO.aarch64 1.38-418.fc28                                  
  perl-IO-Socket-IP.noarch 0.39-5.fc28                           
  perl-MIME-Base64.aarch64 3.15-397.fc28                         
  perl-Net-SSLeay.aarch64 1.85-1.fc28                            
  perl-PathTools.aarch64 3.75-1.fc28                             
  perl-Pod-Escapes.noarch 1:1.07-395.fc28                        
  perl-Pod-Perldoc.noarch 3.28.01-1.fc28                         
  perl-Pod-Simple.noarch 1:3.35-395.fc28                         
  perl-Pod-Usage.noarch 4:1.69-395.fc28                          
  perl-Scalar-List-Utils.aarch64 3:1.49-2.fc28                   
  perl-Socket.aarch64 4:2.029-1.fc28                             
  perl-Storable.aarch64 1:3.15-1.fc28                            
  perl-Term-ANSIColor.noarch 4.06-396.fc28                       
  perl-Term-Cap.noarch 1.17-395.fc28                             
  perl-TermReadKey.aarch64 2.37-7.fc28                           
  perl-Text-ParseWords.noarch 3.30-395.fc28                      
  perl-Text-Tabs+Wrap.noarch 2013.0523-395.fc28                  
  perl-Time-Local.noarch 1:1.280-1.fc28                          
  perl-URI.noarch 1.73-2.fc28                                    
  perl-Unicode-Normalize.aarch64 1.25-397.fc28                   
  perl-constant.noarch 1.33-396.fc28                             
  perl-interpreter.aarch64 4:5.26.3-418.fc28                     
  perl-libnet.noarch 3.11-3.fc28                                 
  perl-libs.aarch64 4:5.26.3-418.fc28                            
  perl-macros.aarch64 4:5.26.3-418.fc28                          
  perl-parent.noarch 1:0.236-395.fc28                            
  perl-podlators.noarch 4.11-1.fc28                              
  perl-srpm-macros.noarch 1-25.fc28                              
  perl-threads.aarch64 1:2.21-2.fc28                             
  perl-threads-shared.aarch64 1.59-1.fc28                        
  python-srpm-macros.noarch 3-30.fc28                            
  python2.aarch64 2.7.15-4.fc28                                  
  python2-asn1crypto.noarch 0.24.0-1.fc28                        
  python2-bcrypt.aarch64 3.1.4-3.fc28                            
  python2-catkin_pkg.noarch 0.4.5-1.fc28                         
  python2-cffi.aarch64 1.11.5-3.fc28                             
  python2-cryptography.aarch64 2.3-1.fc28                        
  python2-dateutil.noarch 1:2.6.1-3.fc28                         
  python2-docutils.noarch 0.14-3.fc28                            
  python2-enum34.noarch 1.1.6-4.fc28                             
  python2-idna.noarch 2.5-4.fc28                                 
  python2-ipaddress.noarch 1.0.18-4.fc28                         
  python2-libs.aarch64 2.7.15-4.fc28                             
  python2-paramiko.noarch 2.4.2-1.fc28                           
  python2-pip.noarch 9.0.3-2.fc28                                
  python2-ply.noarch 3.9-6.fc28                                  
  python2-pyasn1.noarch 0.3.7-2.fc28                             
  python2-pycparser.noarch 2.14-13.fc28                          
  python2-pycurl.aarch64 7.43.0.2-1.fc28                         
  python2-pynacl.aarch64 1.2.0-2.fc28                            
  python2-pyyaml.aarch64 5.1-1.fc28                              
  python2-rosdistro.noarch 0.6.2-3.fc28                          
  python2-rospkg.noarch 1.1.0-3.fc28                             
  python2-setuptools.noarch 40.8.0-1.fc28                        
  python2-six.noarch 1.11.0-3.fc28                               
  python2-vcstools.noarch 0.1.39-3.fc26                          
  python3-catkin_pkg.noarch 0.4.5-1.fc28                         
  python3-dateutil.noarch 1:2.6.1-3.fc28                         
  python3-docutils.noarch 0.14-3.fc28                            
  python3-pyyaml.aarch64 5.1-1.fc28                              
  python3-rosdistro.noarch 0.6.2-3.fc28                          
  python3-rospkg.noarch 1.1.0-3.fc28                             
  qt5-srpm-macros.noarch 5.11.3-1.fc28                           
  rust-srpm-macros.noarch 6-3.fc28                               
  subversion.aarch64 1.11.1-1.fc28                               
  subversion-libs.aarch64 1.11.1-1.fc28                          
  utf8proc.aarch64 2.1.1-2.fc28                                  
  zip.aarch64 3.0-21.fc28                                        
  zstd.aarch64 1.4.0-1.fc28                                      

Upgraded:
  file-libs.aarch64 5.33-10.fc28                                 

Complete!

```

```
pip install -U rosdep rosinstall_generator wstool rosinstall
```

```
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip install --user` instead.
Collecting rosdep
  Downloading https://files.pythonhosted.org/packages/36/b7/8d3b3265034300140a9fec6d62e5d6c53a053beeaf33a53af8d7aeffd78e/rosdep-0.16.1.tar.gz (83kB)
    100% |################################| 92kB 13kB/s 
Collecting rosinstall_generator
  Downloading https://files.pythonhosted.org/packages/d7/bf/491bb4026df6cbc37879426e1a9ffefe8002a1c0b0898b4b79d08f9059a2/rosinstall_generator-0.1.17.tar.gz
Requirement already up-to-date: wstool in /usr/lib/python2.7/site-packages
Collecting rosinstall
  Downloading https://files.pythonhosted.org/packages/38/c5/2c466034d05ad84ca15179d1f496958c6caad9432928bf7436711cc3e64e/rosinstall-0.7.8.tar.gz
Collecting catkin_pkg>=0.4.0 (from rosdep)
  Downloading https://files.pythonhosted.org/packages/1d/6a/6f60a7d45d654b945fcb55ce8f085e2eff2f61137a8577bad06d663e72ee/catkin_pkg-0.4.13.tar.gz (59kB)
    100% |################################| 61kB 15kB/s 
Collecting rospkg>=1.1.8 (from rosdep)
  Downloading https://files.pythonhosted.org/packages/15/4e/ab22127ba3564220d1008e03fe06865b51354761276b3dfe2fb9b219cd1d/rospkg-1.1.10.tar.gz (41kB)
    100% |################################| 51kB 18kB/s 
Collecting rosdistro>=0.7.0 (from rosdep)
  Downloading https://files.pythonhosted.org/packages/e9/0c/1e6127d900b6f828db37df36ef971f249f2429924acdac36a79d5e31ae60/rosdistro-0.7.4.tar.gz (46kB)
    100% |################################| 51kB 26kB/s 
Collecting PyYAML>=3.1 (from rosdep)
  Downloading https://files.pythonhosted.org/packages/e3/e8/b3212641ee2718d556df0f23f78de8303f068fe29cdaa7a91018849582fe/PyYAML-5.1.2.tar.gz (265kB)
    100% |################################| 266kB 18kB/s 
Collecting setuptools (from rosinstall_generator)
  Downloading https://files.pythonhosted.org/packages/b2/86/095d2f7829badc207c893dd4ac767e871f6cd547145df797ea26baea4e2e/setuptools-41.2.0-py2.py3-none-any.whl (576kB)
    100% |################################| 583kB 16kB/s 
Collecting vcstools>=0.1.38 (from wstool)
  Downloading https://files.pythonhosted.org/packages/82/12/6d303e4a882093f16b52538d294596b1683823b62a7dc47c86ce273fae93/vcstools-0.1.42.tar.gz (54kB)
    100% |################################| 61kB 21kB/s 
Collecting docutils (from catkin_pkg>=0.4.0->rosdep)
  Downloading https://files.pythonhosted.org/packages/3a/dc/bf2b15d1fa15a6f7a9e77a61b74ecbbae7258558fcda8ffc9a6638a6b327/docutils-0.15.2-py2-none-any.whl (548kB)
    100% |################################| 552kB 18kB/s 
Collecting python-dateutil (from catkin_pkg>=0.4.0->rosdep)
  Downloading https://files.pythonhosted.org/packages/41/17/c62faccbfbd163c7f57f3844689e3a78bae1f403648a6afb1d0866d87fbb/python_dateutil-2.8.0-py2.py3-none-any.whl (226kB)
    100% |################################| 235kB 11kB/s 
Collecting pyparsing (from catkin_pkg>=0.4.0->rosdep)
  Downloading https://files.pythonhosted.org/packages/11/fa/0160cd525c62d7abd076a070ff02b2b94de589f1a9789774f17d7c54058e/pyparsing-2.4.2-py2.py3-none-any.whl (65kB)
    100% |################################| 71kB 15kB/s 
Collecting six>=1.5 (from python-dateutil->catkin_pkg>=0.4.0->rosdep)
  Downloading https://files.pythonhosted.org/packages/73/fb/00a976f728d0d1fecfe898238ce23f502a721c0ac0ecfedb80e0d88c64e9/six-1.12.0-py2.py3-none-any.whl
Installing collected packages: docutils, six, python-dateutil, pyparsing, catkin-pkg, PyYAML, rospkg, setuptools, rosdistro, rosdep, rosinstall-generator, vcstools, rosinstall
  Found existing installation: docutils 0.14
    DEPRECATION: Uninstalling a distutils installed project (docutils) has been deprecated and will be removed in a future version. This is due to the fact that uninstalling a distutils project will only partially uninstall the project.
    Uninstalling docutils-0.14:
      Successfully uninstalled docutils-0.14
  Found existing installation: six 1.11.0
    Uninstalling six-1.11.0:
      Successfully uninstalled six-1.11.0
  Found existing installation: python-dateutil 2.6.1
    Uninstalling python-dateutil-2.6.1:
      Successfully uninstalled python-dateutil-2.6.1
  Found existing installation: catkin-pkg 0.4.5
    Uninstalling catkin-pkg-0.4.5:
      Successfully uninstalled catkin-pkg-0.4.5
  Running setup.py install for catkin-pkg ... done
  Found existing installation: PyYAML 5.1
    DEPRECATION: Uninstalling a distutils installed project (PyYAML) has been deprecated and will be removed in a future version. This is due to the fact that uninstalling a distutils project will only partially uninstall the project.
    Uninstalling PyYAML-5.1:
      Successfully uninstalled PyYAML-5.1
  Running setup.py install for PyYAML ... done
  Found existing installation: rospkg 1.1.0
    Uninstalling rospkg-1.1.0:
      Successfully uninstalled rospkg-1.1.0
  Running setup.py install for rospkg ... done
  Found existing installation: setuptools 40.8.0
    Uninstalling setuptools-40.8.0:
      Successfully uninstalled setuptools-40.8.0
  Found existing installation: rosdistro 0.6.2
    Uninstalling rosdistro-0.6.2:
      Successfully uninstalled rosdistro-0.6.2
  Running setup.py install for rosdistro ... done
  Found existing installation: rosdep 0.11.4
    Uninstalling rosdep-0.11.4:
      Successfully uninstalled rosdep-0.11.4
  Running setup.py install for rosdep ... done
  Running setup.py install for rosinstall-generator ... done
  Found existing installation: vcstools 0.1.39
    Uninstalling vcstools-0.1.39:
      Successfully uninstalled vcstools-0.1.39
  Running setup.py install for vcstools ... done
  Found existing installation: rosinstall 0.7.7
    Uninstalling rosinstall-0.7.7:
      Successfully uninstalled rosinstall-0.7.7
  Running setup.py install for rosinstall ... done
Successfully installed PyYAML-5.1.2 catkin-pkg-0.4.13 docutils-0.15.2 pyparsing-2.4.2 python-dateutil-2.8.0 rosdep-0.16.1 rosdistro-0.7.4 rosinstall-0.7.8 rosinstall-generator-0.1.17 rospkg-1.1.10 setuptools-41.2.0 six-1.12.0 vcstools-0.1.42

```

```
pip --default-timeout=100 install django
```

```
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip install --user` instead.
Collecting django
  Downloading https://files.pythonhosted.org/packages/3a/c1/dbd77256695f4b4e12b5d2c917a35963db11ce5df19c8ea6cd136b2ed54d/Django-1.11.24-py2.py3-none-any.whl (6.9MB)
    100% |################################| 7.0MB 43kB/s 
Collecting pytz (from django)
  Downloading https://files.pythonhosted.org/packages/87/76/46d697698a143e05f77bec5a526bf4e56a0be61d63425b68f4ba553b51f2/pytz-2019.2-py2.py3-none-any.whl (508kB)
    100% |################################| 512kB 282kB/s 
Installing collected packages: pytz, django
Successfully installed django-1.11.24 pytz-2019.2

```

```
dnf install sudo passwd vim net-tools openssh-server openssh-clients cmake wget
```

```
PATH=$PATH:$HOME/bin:/sbin:/usr/bin:/usr/sbin 
```

```
rosdep init
rosdep update
```

```
mkdir ~/ros_catkin_ws
cd ~/ros_catkin_ws
```

```
rosinstall_generator desktop_full --rosdistro melodic --deps --tar > melodic-desktop-full.rosinstall
wstool init -j8 src melodic-desktop-full.rosinstall
```

```
rosdep install --from-paths src --ignore-src --rosdistro melodic -y --skip-keys=python-opencv
```

```
pip install -U rosdep
rosdep init
rosdep update
```

```
rosdep install -ay --os=fedora:28
```

```
dnf install --skip-broken python-empy console-bridge console-bridge-devel poco-devel boost boost-devel eigen3-devel pyqt4 qt-devel gcc gcc-c++ python-devel sip sip-devel tinyxml tinyxml-devel qt-devel qt5-devel python-qt5-devel sip sip-devel python3-sip python3-sip-devel qconf curl curl-devel gtest gtest-devel lz4-devel urdfdom-devel assimp-devel qhull-devel qhull uuid uuid-devel uuid-c++ uuid-c++-devel libuuid libuuid-devel gazebo gazebo-devel collada-dom collada-dom-devel yaml-cpp yaml-cpp-devel python2-defusedxml python-netifaces pyparsing pydot python-pyqtgraph python2-matplotlib
```

```
rosdep install --from-paths src --ignore-src --rosdistro melodic -y --skip-keys=python-opencv --skip-keys=gazebo9 --skip-keys=libgazebo9-dev
```

```
./src/catkin/bin/catkin_make_isolated --install -DCMAKE_BUILD_TYPE=Release
```

```
dnf install pcllib pcllib-devel
```

```
cp rockchip.repo /etc/yum.repos.d/
dnf update
cp RPM-GPG-KEY-ROCKCHIP /etc/pki/rpm-gpg/
sudo dnf install librockchip_isp

sudo dnf install librockchip_mpp-devel
sudo dnf install librockchip_rga-devel
dnf install librockchip_rtsp-devel curl-devel
```

```
docker run --rm --privileged multiarch/qemu-user-static:register
docker run -it --name toolchain -p 22122:22 -v /home/phy/rk3399/user:/home/user -v /usr/bin/qemu-aarch64-static:/usr/bin/qemu-aarch64-static fedora/rk:1

```

in docker :

```
adduser user
```





