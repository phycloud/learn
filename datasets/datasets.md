# Open source  datassets

## 1 some datasets 
There are some most used datasets:
Cityscapes、Imagenet（ILSVRC）、COCO、PASCAL VOC、CIFAR、MNIST、KITTI、LFW. 

## 2 datasets
+ [BDD100K](https://bdd-data.berkeley.edu/ "Berkeley's datasets, contains 100,000 video sequences, 720p and 30 frames per second.")
  UC Berkeley has opened the largest self-driving dataset to the general public. The huge dataset contains 100,000 video sequences which can be used by engineers and others in the burgeoning industry to further develop self-driving technologies. 
  You can download the dataset called ‘BDD100K’ here. Each video in the dataset is roughly 40 seconds long at decent definition (720p and 30 frames per second). 
  Along with each video GPS information recorded from mobile phones gives an indication of the approximate driving trajectory. All the videos were collected in various locations across the United States. 
  The publicly available videos provide a rich treasure trove to work from as they cover a multitude of different weather conditions from sunny, rainy and even hazy. The balance between day and night time conditions has also been praised. 
+ [ApolloScape](http://apolloscape.auto/index.html "baidu's data.")
+ https://uinedu-my.sharepoint.com/:f:/g/personal/19604_myoffice_site/EiLTzAbNirROrQQF20eupMQB-KpIfZOa7w2YS5MB2ARvSA

