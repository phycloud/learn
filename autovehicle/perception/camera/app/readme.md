# app

* lane camera perception
  类：LaneCameraPerception
  基类：BaseCameraPerception
  成员变量：
  
  + 内部地图
  + 外部地图
  + 车道线检测器指针
  + 车道线后处理器指针
  + 数据标定服务指针
  + 感知参数
  + 车道线标定传感器名称
  + 输出车道线文件状态机
  + 输出数据标定文件状态机
  + 车道线输出目标地址
  + 标定数据输出目标地址

  成员函数：
  + `bool Init(const CameraPerceptionInitOptions &options) override;`
    重载init函数，输入为配置信息，输出init成功，读入Cyber RT根目录，读入config文件，设置车道线标定传感器名称，设置GPU设备ID，调用Initlane和InitCalibrationService 。
  + `void InitLane(const std::string &work_root, base::BaseCameraModelPtr &model,  const app::PerceptionParam &perception_param);`
  输入文件根目录路径、相机数据模型和感知模块参数。完成初始化车道线检测器和后处理器。初始化感知参数，初始化输出文件路径。
  + `void InitCalibrationService(const std::string &work_root, const base::BaseCameraModelPtr model, const app::PerceptionParam &perception_param);`
  输入文件根目录路径、相机数据模型和感知模块参数。初始化标定传感器的名字、内部地图、标定方法、图像高度和宽度，外部参数。
  + `void SetCameraHeightAndPitch(const std::map<std::string, float> name_camera_ground_height_map, const std::map<std::string, float> name_camera_pitch_angle_diff_map, const float &pitch_angle_calibrator_working_sensor);`
  设置高度和间距。
  + `void SetIm2CarHomography(Eigen::Matrix3d homography_im2car);`
  图像矫正
  + `bool GetCalibrationService(BaseCalibrationService **calibration_service);`
   获取标定服务
  + `bool Perception(const CameraPerceptionOptions &options, CameraFrame *frame) override;`
   执行感知程序，输入设置和图像，分配gpu，匹配传感器名称，执行检测程序。输出结果到文件。
  + `std::string Name() const override { return "LaneCameraPerception"; }`
  



