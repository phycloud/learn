## Gitlab auto boot config

Ubuntu 下禁止 Gitlab 开机自启动：

```shell
$ sudo systemctl disable gitlab-runsvdir.service
```

Ubuntu 下启用 Gitlab 开机自启动：

```shell
$ sudo systemctl enable gitlab-runsvdir.service
```



