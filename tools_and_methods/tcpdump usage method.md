# tcpdump 使用方法

## 1.下载安装tcpdump

```sh
sudo apt-get install tcpdump
```

## 2. 基本语法

tcpdump有自己的语法规则，利用这个规则，我们可以很快掌握tcpdump的基本用法。在命令行下输入

`tcpdump -h`

会出现诸如下面的信息

```
tcpdump version 4.2.1
libpcap version 1.1.1
Usage: tcpdump [-aAbdDefhHIKlLnNOpqRStuUvxX] [ -B size ] [ -c count ]
        [ -C file_size ] [ -E algo:secret ] [ -F file ] [ -G seconds ]
        [ -i interface ] [ -M secret ]
        [ -r file ] [ -s snaplen ] [ -T type ] [ -w file ]
        [ -W filecount ] [ -y datalinktype ] [ -z command ]
        [ -Z user ] [ expression ]
```

由上面的信息可以发现，tcpdump的语法可以分为基本选项和表达部分。

## 3.选项介绍

-A 以ASCII格式打印出所有分组，并将链路层的头最小化。
-c 在收到指定的数量的分组后，tcpdump就会停止。
-C 在将一个原始分组写入文件之前，检查文件当前的大小是否超过了参数file_size 中指定的大小。如果超过了指定大小，则关闭当前文件，然后在打开一个新的文件。参数 file_size 的单位是兆字节（是1,000,000字节，而不是1,048,576字节）。
-d 将匹配信息包的代码以人们能够理解的汇编格式给出。
-dd 将匹配信息包的代码以c语言程序段的格式给出。
-ddd 将匹配信息包的代码以十进制的形式给出。
-D 打印出系统中所有可以用tcpdump截包的网络接口。
-e 在输出行打印出数据链路层的头部信息。
-E 用spi@ipaddr algo:secret解密那些以addr作为地址，并且包含了安全参数索引值spi的IPsec ESP分组。
-f 将外部的Internet地址以数字的形式打印出来。
-F 从指定的文件中读取表达式，忽略命令行中给出的表达式。
-i 指定监听的网络接口。
-l 使标准输出变为缓冲行形式，可以把数据导出到文件。
-L 列出网络接口的已知数据链路。
-m 从文件module中导入SMI MIB模块定义。该参数可以被使用多次，以导入多个MIB模块。
-M 如果tcp报文中存在TCP-MD5选项，则需要用secret作为共享的验证码用于验证TCP-MD5选选项摘要（详情可参考RFC 2385）。
-b 在数据-链路层上选择协议，包括ip、arp、rarp、ipx都是这一层的。
-n 不把网络地址转换成名字。
-nn 不进行端口名称的转换。
-N 不输出主机名中的域名部分。例如，‘nic.ddn.mil‘只输出’nic‘。
-t 在输出的每一行不打印时间戳。
-O 不运行分组分组匹配（packet-matching）代码优化程序。
-P 不将网络接口设置成混杂模式。
-q 快速输出。只输出较少的协议信息。
-r 从指定的文件中读取包(这些包一般通过-w选项产生)。
-S 将tcp的序列号以绝对值形式输出，而不是相对值。
-s 从每个分组中读取最开始的snaplen个字节，而不是默认的68个字节。
-T 将监听到的包直接解释为指定的类型的报文，常见的类型有rpc远程过程调用）和snmp（简单网络管理协议；）。
-t 不在每一行中输出时间戳。
-tt 在每一行中输出非格式化的时间戳。
-ttt 输出本行和前面一行之间的时间差。
-tttt 在每一行中输出由date处理的默认格式的时间戳。
-u 输出未解码的NFS句柄。
-v 输出一个稍微详细的信息，例如在ip包中可以包括ttl和服务类型的信息。
-vv 输出详细的报文信息。
-w 直接将分组写入文件中，而不是不分析并打印出来。

-X 输出数据包信息。

## 4.常用选项举例

监听指定wlan0接口信息

tcpdump -i wlan0 

监听3个包后停止

tcpdump -i  wlan0 -c 3

不进行网络地址转换

tcpdump -i wlan0 -c 3 -n

不进行端口号转换

tcpdump -i wlan0 -c 3 -n -nn

输出详细报文信息

tcpdump -i wlan0 -c 3 -n -nn -vv

将数据包信息也输出

tcpdump -i wlan0 -c 3 -n -nn -vv -X

需要输出全部的报文信息而不是截取的默认68字节

tcpdump -i wlan0 -c 3 -n -nn -vv -X -s0

当然，可以简单的这么写tcpdump -i wlan0 -c 3 -nnnvvXs0

## 5.表达式

tcpdump支持正则表达式过滤。

+ 第一种是关于类型的关键字，主要包括**host，net，port**, 例如 host 210.27.48.2，指明 210.27.48.2是一台主机，net 202.0.0.0 指明 202.0.0.0是一个网络地址，net可以表示一个子网，比如 net 192.168.1，port 23 指明端口号是23。如果没有指定类型，缺省的类型host.

+ 第二种是确定传输方向的关键字，主要包括**src , dst ,dst or src, dst and src **,这些关键字指明了传输的方向。举例说明，src 210.27.48.2 ,指明ip包中源地址是210.27.48.2 , dst net 202.0.0.0 指明目的网络地址是202.0.0.0 。如果没有指明方向关键字，则缺省是src or dst关键字。

+ 第三种是协议的关键字，主要包括**fddi,ip,arp,rarp,tcp,udp**等类型。Fddi指明是在FDDI(分布式光纤数据接口网络)上的特定 的网络协议，实际上它是"ether"的别名，fddi和ether具有类似的源地址和目的地址，所以可以将fddi协议包当作ether的包进行处理和 分析。其他的几个关键字就是指明了监听的包的协议内容。如果没有指定任何协议，则tcpdump将会监听所有协议的信息包。

除了这三种类型的关键字之外，其他重要的关键字如下：gateway, broadcast,less,greater,还有三种逻辑运算，取非运算是 'not ' '! ', 与运算是'and','&&;或运算 是'or' ,'||'；这些关键字可以组合起来构成强大的组合条件来满足人们的需要。
举例来说，想要监听目标主机为192.168.1.1ip的udp协议或者端口号为80的信息头

tcpdump -i wlan0 -nn -vv dst 192.168.1.1 and udporport80

在需要使用括号的地方，需要加反斜杠转义。

## 6.常用关键字

tcpdump命令中几种关键字:
第一种:类型关键字,包括:host,net,port
第二种:传输方向关键字,包括:src,dst  
第三种:协议关键字,包括: ip,arp,tcp,udp等类型                                          
第四种:其他关键字,包括:gateway,broadcast,less,greater,not,!,and,&&,or,||

【备注说明】

1) 抓取回环网口的包：        $ tcpdump -i lo
 2) 防止包截断的方法：        $ tcpdump -s 0
 3) 以数字显示主机及端口：    $ tcpdump -n

【命令浅解】

$ tcpdump tcp -i eth1 -t -s0 -c 100 and dst port ! 22 and src net 192.168.1.0/24 -w ./target.cap

1)tcp:                     #  ip,icmp,arp,rarp,udp这些选项要放第一个参数，用来过滤数据报的类型
2)-i eth1                  # 只抓经过网口eth1的包
3)-t                       # 不显示时间戳
4)-s 0                     # 抓取数据包时默认抓取长度为68字节。加上-s 0 后可以抓到完整的数据包
5)-c 100                   # 只抓取100个数据包
6)dst port ! 22            # 不抓取目标端口是22的数据包
7)src net 192.168.1.0/24   # 数据包的源网络地址为192.168.1.0/24
8)-w ./target.cap          # 保存成cap文件，方便用wireshark工具进行分析

【其他命令】

$ tcpdump host 192.168.0.1 and /(192.168.0.2 or 192.168.0.3 /)  # 截取主机1与主机2或3之间的通信包

$ tcpdump ip host 192.168.0.1 and ! 192.168.0.2         # 截取主机1除了和主机2之外所有主机通信的ip包

$ tcpdump tcp port 23 host 210.27.48.1                  # 截取主机192.168.0.1接收或发出的telnet包

$ tcpdump -i eth0 host ! 192.168.0.1 and ! 192.168.0.2 and dst port 80 # 截获除了主机1、2外访问本机http端口的数据包

主要语法

```sh
过滤主机/IP：
    tcpdump -i eth1 host 172.16.7.206
    抓取所有经过网卡1，目的IP为172.16.7.206的网络数据

过滤端口：
    tcpdump -i eth1 dst port 1234
    抓取所有经过网卡1，目的端口为1234的网络数据

过滤特定协议：
    tcpdump -i eth1 udp
    抓取所有经过网卡1，协议类型为UDP的网络数据

抓取本地环路数据包
    tcpdump -i lo udp 抓取UDP数据
    tcpdump -i lo udp port 1234 抓取端口1234的UDP数据
    tcpdump -i lo port 1234 抓取端口1234的数据

特定协议特定端口：
    tcpdump udp port 1234
    抓取所有经过1234端口的UDP网络数据

抓取特定类型的数据包：
    tcpdump -i eth1 ‘tcp[tcpflags] = tcp-syn’
    抓取所有经过网卡1的SYN类型数据包
    tcpdump -i eth1 udp dst port 53
    抓取经过网卡1的所有DNS数据包（默认端口）

逻辑语句过滤：
    tcpdump -i eth1 ‘((tcp) and ((dst net 172.16) and (not dst host 192.168.1.200)))’
    抓取所有经过网卡1，目的网络是172.16，但目的主机不是192.168.1.200的TCP数据

抓包存取：
    tcpdump -i eth1 host 172.16.7.206 and port 80 -w /tmp/xxx.cap
    抓取所有经过网卡1，目的主机为172.16.7.206的端口80的网络数据并存储
```






