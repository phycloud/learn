# Packet Loss Solution for Long Range Millimeter Wave Radar ARS-408-21

## 1 问题描述

主要解决车载毫米波雷达ARS-408-21采集数据时出现长时间丢数据的问题。效果如图所示，蓝色条带表示两路毫米波雷达的数据接收情况，白色区域表示数据丢失，其中出现长时间丢数据的现象，最长时间达到十秒。

![](.\res\修改前.jpg)

## 2 分析原因

数据采集系统部署在工控机上，工控机通过有线网连接交换机，交换机连接CANET模块，CANET模块连接毫米波雷达。则数据传输流程如下：

·毫米波雷达 --> CANET模块 --> 工控机网卡 --> ros毫米波雷达驱动·

#### 可能出现原因：

+ 驱动程序问题
+ CANET将CAN协议转化为UDP协议时出现问题
+ 毫米波雷达传感器硬件问题 

# 3 测试方案

检测方案：

+ 工控机上部署tcpdump工具，通过tcpdump抓取IP为192.168.110.101，端口为4001的UDP数据，即毫米波雷达的数据；
+ 在测试出现问题的路况下，同时运行ROS radar driver和tcpdump，分别保存数据到本地。
+ 另外开发测试程序，完全模拟ROS radar driver的数据解析方法，从tcpdump抓取的UDP数据。
+ cap中解析传感器消息，并与ROS保存的bag消息对比。

![](.\res\测试流程.png)

# 4 分析数据

### 分析毫米波雷达的消息格式：

毫米波雷达CAN总线消息每帧13个字节，通过帧ID标记每帧的数据含义，数据采集过程中主要用到0x060a、0x060b、0x060c、0x060四种数据。其中

ID=0x060a 表示接下来将要连续发送的数据帧数；

ID=0x060b、0x060c、0x060d 的数据共同组成一条传感器数据。

举例：

ID=0x060a：数据长度为3

ID=0x060b：数据1

ID=0x060b：数据2

ID=0x060b：数据3

ID=0x060c：数据1

ID=0x060c：数据2

ID=0x060c：数据3

ID=0x060d：数据1

ID=0x060d：数据2

ID=0x060d：数据3

#### 分析UDP包的帧格式

Dst (Diestination Mac Address):  6 bytes

Src (Source Mac Address): 6 bytes

IP Type: 2 bytes, 0x0800 for IPv4

Internet Protocol Version: 1 byte,  0100 for Version 4,  0101 for Header length 20 bytes (5)

Differentiated Service Field: 1 byte, 0x00

Total Length: 2bytes, except Dst and Src

Identification: 2 bytes

Flags: 2 bytes

Time to live: 1 byte, 255

Protocol: 1 bytes, UDP (17)

Header Checksum: 2 bytes

Source IP: 4 bytes

Destination IP: 4 bytes

Source Port: 2 bytes

Destination Port: 2 bytes

Length: 2 bytes (Src & Dst ports, Length, Checksum, Data )

Checksum: 2 bytes

Data: (Length-8) bytes

#### 根据帧格式信息初步定位问题位置

+ 首先分析CAN数据帧中ID=0x60a的数据。通过打印相邻两个60a数据帧的时间间隔，发现时间间隔均小于0.12秒，由此推测丢包不是因为60a数据帧丢失；
+ 分析毫米波雷达CAN总线发出数据的序列顺序，可以打印丢包时刻，各种类型的数据帧接收情况：

![](.\res\CAN帧序列.png)

+ 通过对比ROS抓取的传感器消息和tcpdump抓取的UDP消息，丢包时刻基本吻合，由此得出结论，数据在传入工控机时已经出现丢包；

  分析tcpdump抓取的毫米波雷达的消息：

  [1560759824.151670]-LOST! Expect Num:97, Recv [numB numC numD]:[97 97 67]

  [1560759824.221488]-LOST! Expect Num:95, Recv [numB numC numD]:[95 95 66]

  [1560759824.301539]-LOST! Expect Num:95, Recv [numB numC numD]:[95 95 90]

  [1560759824.380012]-LOST! Expect Num:99, Recv [numB numC numD]:[99 99 62]

  [1560759824.454943]-LOST! Expect Num:98, Recv [numB numC numD]:[98 98 68]

  [1560759824.528924]-LOST! Expect Num:99, Recv [numB numC numD]:[99 99 67]

+ 通过分析，丢包情况均是由于ID=0x060d的数据帧数量没有达到0x060a数据帧中所指明的数量，导致解析出现问题。

  # 5  解决办法

  原始驱动程序，在接收数据没有达到0x060a中指明的数量时，直接丢弃所有数据，导致长时间数据空白。修改方法：

  + 在接收数据完全时，依赖0x060a数据帧中所指明的数据帧数量解析传感器消息，

  + 在接收数据不完全时，根据实际接收到的数据帧数量解析传感器消息。

  效果

  ![](.\res\修改后.jpg)

