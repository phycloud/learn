# flow



```flow
# 先自定义变量,然后画图 
st=>start: 开始 
e=>end: 结束 
op=>operation: 输入x 
sub=>subroutine: 是否重新输入 
cond1=>condition: x>0? 
cond2=>condition: yes/no 
io=>inputoutput: 输出x 
st(right)->op->cond1 
cond1(yes)->io(right)->e 
cond1(no)->sub(right)->cond2() 
cond2(yes, right)->op 
cond2(no)->e
```





```
A-->B
```





```mermaid
graph LR
cyber[cyber component]
calibration[calibration]
canbus[canbus]
control[control]
dreamview[dreamview]
guardian[guardian ]
localization[localization]
map[map]
monitor[monitor]
perception[perception]
prediction[prediction]
planning[planning]
routing[routing]
transform[transform]
v2x[v2x]

fusion(fusion)
fusion_camera(fusion camera)
lane_detection(lane detection)
lidar_output(lidar output)
radar_detection(radar detection)
recognition(recognition)
segmentation(segmentation)
trafficlight(trafficlight)

cyber-->calibration
cyber-->canbus
cyber-->control
cyber-->dreamview
cyber-->guardian
cyber-->localization
cyber-->map
cyber-->perception
cyber-->routing
cyber-->prediction
cyber-->planning
cyber-->monitor
cyber-->transform
cyber-->v2x

perception-->fusion
perception-->fusion_camera
perception-->lane_detection
perception-->lidar_output
perception-->radar_detection
perception-->recognition
perception-->segmentation
perception-->trafficlight
```





```mermaid
gantt
    dateFormat  YYYY-MM-DD
    title Adding GANTT diagram functionality to mermaid

    section A section
    Completed task            :done,    des1, 2014-01-06,2014-01-08
    Active task               :active,  des2, 2014-01-09, 3d
    Future task               :         des3, after des2, 5d
    Future task2               :         des4, after des3, 5d

    section Critical tasks
    Completed task in the critical line :crit, done, 2014-01-06,24h
    Implement parser and jison          :crit, done, after des1, 2d
    Create tests for parser             :crit, active, 3d
    Future task in critical line        :crit, 5d
    Create tests for renderer           :2d
    Add to mermaid                      :1d

    section Documentation
    Describe gantt syntax               :active, a1, after des1, 3d
    Add gantt diagram to demo page      :after a1  , 20h
    Add another diagram to demo page    :doc1, after a1  , 48h

    section Last section
    Describe gantt syntax               :after doc1, 3d
    Add gantt diagram to demo page      : 20h
    Add another diagram to demo page    : 48h
```







