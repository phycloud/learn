# install arm64v8/ubuntu docker on x86_64 server

in a terminal run:

```bash
sudo docker search arm64
```

output

```bash
NAME                                   DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
arm64v8/alpine                         A minimal Docker image based on Alpine Linux…   40                          
arm64v8/ubuntu                         Ubuntu is a Debian-based Linux operating sys…   26                         
arm64v8/debian                         Debian is a Linux distribution that's compos…   20                                      
arm64v8/python                         Python is an interpreted, interactive, objec…   17                                      
arm64v8/nginx                          Official build of Nginx.                        16                                      
arm64v8/nextcloud                      A safe home for all your data                   13                                      
arm64v8/node                           Node.js is a JavaScript-based platform for s…   10                                      
arm64v8/postgres                       The PostgreSQL object-relational database sy…   7                                       
arm64v8/php                            While designed for web development, the PHP …   6                                       
arm64v8/redis                          Redis is an open source key-value store that…   6                                       
arm64v8/openjdk                        OpenJDK is an open-source implementation of …   6                                       
arm64v8/docker                         Docker in Docker!                               6                                       
arm64v8/ros                            The Robot Operating System (ROS) is an open …   5                                       
arm64v8/golang                         Go (golang) is a general purpose, higher-lev…   4                                       
arm64v8/mongo                          MongoDB document databases provide high avai…   4                                       
arm64v8/buildpack-deps                 A collection of common build dependencies us…   3                                       
arm64v8/busybox                        Busybox base image.                             3                                       
arm64v8/ruby                           Ruby is a dynamic, reflective, object-orient…   2                                       
arm64v8/tomcat                         Apache Tomcat is an open source implementati…   2                                       
arm64v8/wordpress                      The WordPress rich content management system…   1                                       
arm64v8/drupal                         Drupal is an open source content management …   1                                       
arm64v8/haxe                           Haxe is a modern, high level, static typed p…   0                                       
arm64v8/hylang                         Hy is a Lisp dialect that translates express…   0                                       
arm64v8/joomla                         Joomla! is an open source content management…   0                                       
troyfontaine/arm64v8_min-alpinelinux   Minimal 64-bit ARM64v8 Alpine Linux Image       0 
```

run

```bash
sudo docker pull arm64v8/ubuntu:16.04
```

output

```bash
16.04: Pulling from arm64v8/ubuntu
6d4be76c88d2: Pull complete 
ecc699f7081c: Pull complete 
a87b49e9f959: Pull complete 
b5d927bb280c: Pull complete 
Digest: sha256:6c6dc3e0c188da27e29edb852d03034e6b7d50a0f10b8ff296220c3a141b703a
Status: Downloaded newer image for arm64v8/ubuntu:16.04
```

run

```bash
sudo docker images
```

output

```bash
REPOSITORY            TAG                                            IMAGE ID            CREATED             SIZE
arm64v8/ubuntu        16.04                                          b63658c0b8e9        7 days ago          110MB
zabbix/zabbix-agent   latest                                         c67eb0e5bd2f        2 weeks ago         15.2MB
```

go to https://github.com/multiarch/qemu-user-static/releases and download [qemu-aarch64-static](https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0-5/qemu-aarch64-static "qemu for arm64") or [qemu-aarch64-static.tar.gz](https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0-5/qemu-aarch64-static.tar.gz "qemu for arm64 .tag.gz") .

run

```bash
wget https://github.com/multiarch/qemu-user-static/releases/download/v4.0.0-5/qemu-aarch64-static
```

```bash
sudo cp qemu-aarch64-static /usr/bin/
sudo chmod +x /usr/bin/qemu-aarch64-static
```

```bash
sudo docker run --rm --privileged multiarch/qemu-user-static:register
```

output

```bash
Unable to find image 'multiarch/qemu-user-static:register' locally
register: Pulling from multiarch/qemu-user-static
ee153a04d683: Pull complete 
2aa78cb56602: Pull complete 
96aa3d7b9b30: Pull complete 
8d669bf48302: Pull complete 
Digest: sha256:f755311f4122e4a464d191d50c360512f712293180723bb1e871b3b2e8d5f076
Status: Downloaded newer image for multiarch/qemu-user-static:register
Setting /usr/bin/qemu-alpha-static as binfmt interpreter for alpha
Setting /usr/bin/qemu-arm-static as binfmt interpreter for arm
Setting /usr/bin/qemu-armeb-static as binfmt interpreter for armeb
Setting /usr/bin/qemu-sparc32plus-static as binfmt interpreter for sparc32plus
Setting /usr/bin/qemu-ppc-static as binfmt interpreter for ppc
Setting /usr/bin/qemu-ppc64-static as binfmt interpreter for ppc64
Setting /usr/bin/qemu-ppc64le-static as binfmt interpreter for ppc64le
Setting /usr/bin/qemu-m68k-static as binfmt interpreter for m68k
Setting /usr/bin/qemu-mips-static as binfmt interpreter for mips
Setting /usr/bin/qemu-mipsel-static as binfmt interpreter for mipsel
Setting /usr/bin/qemu-mipsn32-static as binfmt interpreter for mipsn32
Setting /usr/bin/qemu-mipsn32el-static as binfmt interpreter for mipsn32el
Setting /usr/bin/qemu-mips64-static as binfmt interpreter for mips64
Setting /usr/bin/qemu-mips64el-static as binfmt interpreter for mips64el
Setting /usr/bin/qemu-sh4-static as binfmt interpreter for sh4
Setting /usr/bin/qemu-sh4eb-static as binfmt interpreter for sh4eb
Setting /usr/bin/qemu-s390x-static as binfmt interpreter for s390x
Setting /usr/bin/qemu-aarch64-static as binfmt interpreter for aarch64
Setting /usr/bin/qemu-aarch64_be-static as binfmt interpreter for aarch64_be
Setting /usr/bin/qemu-hppa-static as binfmt interpreter for hppa
Setting /usr/bin/qemu-riscv32-static as binfmt interpreter for riscv32
Setting /usr/bin/qemu-riscv64-static as binfmt interpreter for riscv64
Setting /usr/bin/qemu-xtensa-static as binfmt interpreter for xtensa
Setting /usr/bin/qemu-xtensaeb-static as binfmt interpreter for xtensaeb
Setting /usr/bin/qemu-microblaze-static as binfmt interpreter for microblaze
Setting /usr/bin/qemu-microblazeel-static as binfmt interpreter for microblazeel
Setting /usr/bin/qemu-or1k-static as binfmt interpreter for or1k

```

run : create and start a container

```bash
sudo docker run -it -v /usr/bin/qemu-aarch64-static:/usr/bin/qemu-aarch64-static arm64v8/ubuntu:16.04
```

output

```bash
Unable to find image 'arm64v8/ubuntu:latest' locally
latest: Pulling from arm64v8/ubuntu
e5e9de242ab4: Pull complete 
60d7edb2b4b3: Pull complete 
af2d5adc6d48: Pull complete 
07198cd8e218: Pull complete 
Digest: sha256:be2ef80501adc5a13c4a58a8045923d4e64b691aaa7e6e470c14e88a45845d0d
Status: Downloaded newer image for arm64v8/ubuntu:latest
```

running OK!

start/stop/restart a container

```
sudo docker start/stop/restar <container_id>  
```

attach into a container

```
sudo docker attach <container_id>
```

```
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial main multiverse restricted universe
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial-security main multiverse restricted universe
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial-updates main multiverse restricted universe
deb http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial-backports main multiverse restricted universe
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial main multiverse restricted universe
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial-security main multiverse restricted universe
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial-updates main multiverse restricted universe
deb-src http://mirrors.tuna.tsinghua.edu.cn/ubuntu-ports/ xenial-backports main multiverse restricted universe
```

```
sudo docker commit c0f84c949bb9 ubuntu-base-ros-kinetic
```



