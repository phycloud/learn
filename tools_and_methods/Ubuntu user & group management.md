# Ubuntu user & group management

### 1 查看Ubuntu上所有的组，

键入命令groupmod并（连按三次tab键）

groupmod <连按三次tab键>

### 2 添加用户到已存在的组

运行如下指令

sudo adduser [username] [groupname]

验证用户的相关组，运行下列命令：

id [username]

### 3 添加新用户

按Ctrl+Alt+t打开终端，输入sudo adduser username（所要创建的新用户名称），系统会显示以下信息：

#### 切换用户

在创建了新用户之后，千万不要急匆匆地切换到新用户，先注意一下分组问题，在root权限下修改/etc/sudoers

vi /etc/sudoers

修改此处：

\# User privilege specification  

root    ALL=(ALL:ALL) ALL  

**username    ALL=(ALL:ALL) ALL  （添加这一行，username是新创建的用户名）**

sudoers权限为0440，只有root才能读，所以需要在root权限下强制保存退出（wq！）

1.从root切换到username

su username

（可能需要输入root密码）

2.从username切换到root

sudo su

（需要输入username的密码）

### 4 查看所有用户组

```ruby
cat /etc/group

ssl-cert:x:110:postgres
```

### 5 查看所有用户

sudo cat /etc/shadow

postgres:$6$m8anDHdE$FDY4j0CdAbgeLOM90EH1xCW/IMqHEZwM87sepyHHjUYccdmFOCVaFealGTd2zGBVfDV.AR9CWTlGz0Sw/JivL1:15910:0:999

要添加新用户到sudo，最简单的方式就是使用 usermod 命令。运行
$sudo usermod -G admin username
这就你要作的，然而，如果用户已经是其他组的成员，你需要添加 -a 这个选项，象这样
$sudo usermod -a -G admin username