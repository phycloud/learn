### Check out, review, and merge locally

 **Step 1.** Fetch and check out the branch for this merge request 

```
git fetch origin
git checkout -b 2-segmentation_fault origin/2-segmentation_fault
```

 **Step 2.** Review the changes locally 

 **Step 3.** Merge the branch and fix any conflicts that come up 

```
git fetch origin
git checkout origin/master
git merge --no-ff 2-segmentation_fault
```

 **Step 4.** Push the result of the merge to GitLab 

```
git push origin master
```

 **Tip:** You can also checkout merge requests locally by [following these guidelines](http://192.168.3.224:8081/help/user/project/merge_requests/index.md#checkout-merge-requests-locally). 