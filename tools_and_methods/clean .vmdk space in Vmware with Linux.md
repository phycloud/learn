# clean *.vmdk space in Vmware with Linux

1 open the Linux OS.

In Linux: write 0 into remain space and remove it with root.

```shell
sudo dd if=/dev/zero of=/big bs=10M

sudo rm -rf /big
```

2 close the Linux OS.

3 use vmware-vdiskmanager.exe.

in cmd :

```powershell
cd C:\Program Files (x86)\VMware\VMware Workstation
vmware-vdiskmanager.exe -k E:\ubuntu-16.04.6\Ubuntu-16.04.6.vmdk
```





