# cmake error Project 'image_geometry' specifies '/usr/include/opencv' as an include dir,
  which is not found

the error:

```
CMake Error at /opt/ros/melodic/share/image_geometry/cmake/image_geometryConfig.cmake:113 (message):
  Project 'image_geometry' specifies '/usr/include/opencv' as an include dir,
  which is not found.  It does neither exist as an absolute directory nor in
  '/opt/ros/melodic//usr/include/opencv'.  Check the website
  'http://www.ros.org/wiki/image_geometry' for information and consider
  reporting the problem.

```

reason:

in /opt/ros/melodic/share/image_geometry/cmake/image_geometryConfig.cmake:113

```
set(cv_bridge_FOUND_CATKIN_PROJECT TRUE)
if(NOT "include;/usr/include/opencv;/usr/include " STREQUAL " ")
  set(cv_bridge_INCLUDE_DIRS "")
  set(_include_dirs "include;/usr/include/opencv;/usr/include")
```

It find opencv headers in '/usr/include' or '/usr/include/opencv'. If opencv is installed by source with default configures, it is installed in '/usr/local/include/opencv'. The 'image_geometryConfig.cmake' should be changed as following:


```
set(cv_bridge_FOUND_CATKIN_PROJECT TRUE)
if(NOT "include;/usr/local/include/opencv" STREQUAL " ")
  set(cv_bridge_INCLUDE_DIRS "")
  set(_include_dirs "/usr/local/include/opencv;/usr/include;/usr/local/include")
```

