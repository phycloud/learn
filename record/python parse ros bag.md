# python parse ros bag

read image in bag

```
#!/bin/python
# -*- coding: UTF-8 -*-

import roslib; #roslib.load_manifest(PKG)
import rosbag
import numpy as np
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from cv_bridge import CvBridgeError

class ImageCreator():
    def __init__(self):
        self.bridge = CvBridge()
        with rosbag.Bag('shougang_01.bag', 'r') as bag:  #要读取的bag文件；
            for topic,msg,t in bag.read_messages():
                if topic == "/usb_cam_left/image_raw/compressed":
                    try:
                        # cv_image = self.bridge.imgmsg_to_cv2(msg)
                        np_arr = np.fromstring(msg.data, np.uint8)
                        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)

                        timestr = "%.6f" %  msg.header.stamp.to_sec()
                        #%.6f表示小数点后带有6位，可根据精确度需要修改；
                        image_name = './sg/'+timestr+ ".jpg" #图像命名：时间戳.jpg
                        cv2.imwrite(image_name, image_np)
                    except CvBridgeError as e:
                        print e    
                    
if __name__ == '__main__':
    try:
        image_creator = ImageCreator()
    except rospy.ROSInterruptException:
        pass

```



