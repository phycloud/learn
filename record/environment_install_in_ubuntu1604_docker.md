# for ubuntu 16.04

under root user:

```bash
apt install nano
```

```bash
nano /etc/apt/source.list
```

sources

```bash
deb http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial main multiverse restricted universe
deb http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial-security main multiverse restricted universe
deb http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial-updates main multiverse restricted universe
deb http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial-backports main multiverse restricted universe
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial main multiverse restricted universe
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial-security main multiverse restricted universe
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial-updates main multiverse restricted universe
deb-src http://mirrors.ustc.edu.cn/ubuntu-ports/ xenial-backports main multiverse restricted universe
```

```bash
apt install vim
apt install net-tools
apt install iputils-ping
apt install iproute2
apt install openssh-server
apt install build-essential gcc cmake
```

exit docker

 ```
sudo docker ps -l
 ```

output

```
CONTAINER ID        IMAGE                  COMMAND             CREATED             STATUS                       PORTS               NAMES
ae05439b9cfb        arm64v8/ubuntu:16.04   "/bin/bash"         10 minutes ago      Exited (127) 8 minutes ago                       boring_minsky
```

```
sudo docker commit ae05439b9cfb ubuntu/net
```

```
docker cp <containerId>:/file/path/within/container /host/path/target
```

```
docker cp foo.txt mycontainer:/foo.txt
docker cp mycontainer:/foo.txt foo.txt
```

```
sudo docker images
```

```
sudo docker run -p 8000:22 -it ubuntu:16.04 /bin/bash
```



