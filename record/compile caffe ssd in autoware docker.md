# compile caffe ssd in autoware docker

install libs

```
apt install liblmdb-dev libleveldb-dev libsnappy-dev libatlas-base-dev 
```

update cmake >= 3,14

## Problem

I was trying to build **BVLC Caffe** from source as described [here](https://codeyarns.github.io/tech/2017-02-22-how-to-build-caffe.html) on Ubuntu 18.04 with CUDA 10.0. CMake ended with this error:

```
Please set them or make sure they are set and tested correctly in the CMake files:
CUDA_cublas_device_LIBRARY (ADVANCED)
```

## Solution

This problem seems to occur with a combination of CUDA 10.0 or later  and a CMake version that is older than 3.13. I upgraded to CMake 3.14.0  as described [here](https://codeyarns.github.io/tech/2019-03-20-how-to-install-cmake.html) and the problem was solved.