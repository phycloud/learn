# Apollo software modules



## 1 perception 感知

The Perception module has been upgraded completely to handle comprehensive sensor fusion of our brand-new sensor suite and also keep up with the brand new scenario-based planning.

The perception module incorporates the capability of using multiple cameras, radars (front and rear) and LiDARs to recognize obstacles and fuse their individual tracks to obtain a final track list. The obstacle sub-module detects, classifies and tracks obstacles. This sub-module also predicts obstacle motion and position information (e.g., heading and velocity). For lane line, we construct lane instances by postprocessing lane parsing pixels and calculate the lane relative location to the ego-vehicle (L0, L1, R0, R1, etc.).

The general architecture of the perception module is shown:

![Apollo3.5_perception_sensor_based](C:\Users\peihy\Documents\bitbucket\Apollo software mudules\pictures\Apollo3.5_perception_sensor_based.png)

The detailed perception modules are displayed below.

![Apollo3.5_perception_detail](C:\Users\peihy\Documents\bitbucket\Apollo software mudules\pictures\Apollo3.5_perception_detail.png)

#### Input

The perception module inputs are:

- 128 channel LiDAR data (cyber channel /apollo/sensor/velodyne128)
- 16 channel LiDAR data (cyber channel /apollo/sensor/lidar_front, lidar_rear_left, lidar_rear_right)
- Radar data (cyber channel /apollo/sensor/radar_front, radar_rear)
- Image data (cyber channel /apollo/sensor/camera/front_6mm, front_12mm)
- Extrinsic parameters of radar sensor calibration (from YAML files)
- Extrinsic and Intrinsic parameters of front camera calibration (from YAML files)
- Velocity and Angular Velocity of host vehicle (cyber channel /apollo/localization/pose)

#### Output

The perception module outputs are:

1. The 3D obstacle tracks with the heading, velocity and classification information (cyber channel /apollo/perception/obstacles)
2. The output of traffic light detection and recognition (cyber channel /apollo/perception/traffic_light)



## 2 prediction 预测

The Prediction module studies and predicts the behavior of all the obstacles detected by the perception module. Prediction receives obstacle data along with basic perception information including positions, headings, velocities, accelerations, and then generates predicted trajectories with probabilities for those obstacles.

Note: `The Prediction module only predicts the behavior of obstacles and not the EGO car. The Planning module plans the trajectory of the EGO car.`

#### Input

- Obstacles information from the perception module
- Localization information from the localization module
- Planning trajectory of the previous computing cycle from the planning module

#### Output

- Obstacles annotated with predicted trajectories and their  priorities. Obstacle priority is now calculated as individual scenarios  are prioritized differently. The priorities include: ignore, caution and  normal (default)

#### Functionalities

Based on the figure below, the prediction module comprises of 4 main  functionalities: Container, Scenario, Evaluator and Predictor.   Container, Evaluator and Predictor existed in Apollo 3.0. In Apollo 3.5,  we introduced the Scenario functionality as we have moved towards a  more scenario-based approach for Apollo's autonomous driving  capabilities. ![prediction](C:\Users\peihy\Documents\bitbucket\Apollo software mudules\pictures\prediction.png)

#### Container

Container stores input data from subscribed channels. Current supported inputs are **perception obstacles**, **vehicle localization** and **vehicle planning**.

#### Scenario

The Scenario sub-module analyzes scenarios that includes the ego vehicle. Currently, we have two defined scenarios:

- **Cruise** : this scenario includes Lane keeping and following
- **Junction** : this scenario involves junctions. Junctions can either have traffic lights and/or STOP signs

#### Obstacles

- **Ignore**: these obstacles will not affect the ego car's trajectory and can be safely ignored (E.g. the obstacle is too far away)
- **Caution**: these obstacles have a high possibility of interacting with the ego car
- **Normal**: the obstacles that do not fall under ignore or caution are placed by default under normal

#### Evaluator

The Evaluator predicts path and speed separately for any given obstacle. An evaluator evaluates a path by outputting a probability for it (lane sequence) using the given model stored in *prediction/data/*.

There exists 5 types of evaluators, two of which were added in Apollo  3.5. As Cruise and Junction scenarios have been included, their  corresponding evaluators (Cruise MLP and Junction MLP) were added as  well. The list of available evaluators include:

- **Cost evaluator**: probability is calculated by a set of cost functions
- **MLP evaluator**: probability is calculated using an MLP model
- **RNN evaluator**: probability is calculated using an RNN model
- **Cruise MLP + CNN-1d evaluator**: probability is calculated using a mix of MLP and CNN-1d models for the cruise scenario
- **Junction MLP evaluator**: probability is calculated using an MLP model for junction scenario
- **Junction Map evaluator**: probability is calculated  using an semantic map-based CNN model for junction scenario. This  evaluator was created for caution level obstacles
- **Social Interaction evaluator**: this model is used for  pedestrians, for short term trajectory prediction. It uses social LSTM.  This evaluator was created for caution level obstacles

#### Predictor

Predictor generates predicted trajectories for obstacles. Currently, the supported predictors include:

- **Empty**: obstacles have no predicted trajectories
- **Single lane**: Obstacles move along a single lane in highway navigation mode. Obstacles not on lane will be ignored.
- **Lane sequence**: obstacle moves along the lanes
- **Move sequence**: obstacle moves along the lanes by following its kinetic pattern
- **Free movement**: obstacle moves freely
- **Regional movement**: obstacle moves in a possible region
- **Junction**: Obstacles move toward junction exits with high probabilities
- **Interaction predictor**: compute the likelihood to  create posterior prediction results after all evaluators have run. This  predictor was created for caution level obstacles

#### Prediction Architecture

The prediction module estimates the future motion trajectories for  all perceived obstacles. The output prediction message wraps the  perception information. Prediction both subscribes to and is triggered  by perception obstacle messages, as shown below:

## 3 Planning 轨迹规划

The previous versions of Apollo, including Apollo 3.0 currently use the same configuration and parameters to plan different driving scenarios. This approach although linear and easy to implement, was not flexible or scenario specific. As Apollo matures and takes on different road conditions and driving use cases, we felt the need to move to a more modular, scenario specific and wholistic approach for planning its trajectory. In this approach, each driving use case is treated as a different driving scenario. This is useful because an issue now reported in a particular scenario can be fixed without affecting the working of other scenarios as opposed to the previous versions, wherein an issue fix affected other driving use cases as they were all treated as a single driving scenario.

### Driving Scenarios

There are 3 main driving scenarios that we will focus on, namely:

#### Lane Follow - Default

As seen in the figure below, the lane-follow scenario, our default  driving scenario, includes but is not limited to driving in a single  lane (like cruising) or changing lane, following basic traffic  convention or basic turning.

`Note: While the functionality of side pass still exists, it has now been made universal rather than limiting it to a type of scenario. The side-pass feature is incorporated as part of the path-bounds decider task. You can choose to turn it on or off by properly configuring the path-lane-borrow decider task. For example, if you want the vehicle to be agile, then turn side-pass on for all scenarios; if you feel it not safe to side-pass in intersections, then turn it off for those related scenarios.`

#### Intersection

The new intersection scenario includes STOP Signs, Traffic Lights and  Bare Intersections which do not have either a light or a sign.

#### STOP Sign

There are two separate driving scenarios for STOP signs:

- **Unprotected**: In this scenario, the car is expected  to navigate through a crossroad having a two-way STOP. Our ADC therefore  has to creep through and gauge the crossroad's traffic density before  continuing onto its path.

![unprotected1](C:\Users\peihy\Documents\bitbucket\Apollo software mudules\pictures\unprotected1.png)







## 4 Control 控制

## 5 Relative Map 相对地图

## 6 Location 定位

## 7 Navigator 导航

##  8 HDMap 高精度地图

## 9 CANBus CAN总线

## 10 Guardian 

## 11 Monitor

## 12 HMI 





